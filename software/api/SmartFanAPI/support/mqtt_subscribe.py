from paho.mqtt import client as mqtt_client
import random, time, ssl

broker = "mqtt.flespi.io"
port = 8883
topic = "/topic/test"
client_id = "python-mqtt-{}".format(random.randint(0, 100))
username = "FlespiToken 8WO1QAhHm97ElFYUsN7DMOCEFhmzuHistUh9ipFuoCXy5LinodO0SftlkDKEaRWr"
password = ""

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if (rc == 0):
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.tls_set("certs/mqtt_flespi_io.pem", tls_version=ssl.PROTOCOL_TLSv1_2)
    client.tls_insecure_set(True)
    client.connect(broker, port)
    
    return client

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print("Received '{}' from '{}' topic".format(msg.payload.decode(), msg.topic))
    
    client.subscribe(topic)
    client.on_message = on_message

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()

if (__name__ == '__main__'):
    run()