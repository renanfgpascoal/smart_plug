# Project Specs
This document describes specifications for Smart Plug project.<br>
Showing PO's suggestions for a user demand, a proposal of solution and tasks that device should execute this solution.

| Owner | Acceptance date |
|-------|------------|
| Renan Pascoal | 14/07/2021 |
## 1 - User stories
| Priority | Stories |
|----------|---------|
| 0 | As a plug-in devices user, I want to turn them on and off remotely.|
| 1 | As a notebook user, I want to automatically turn off my charger when battery is full. |

## 2 - Project proposals
1. An electronic device connected between plug-in devices and outlets. It can power on and off loads remotely trough Wi-Fi local network;
2. Same device as above, it detects when notebook battery is full and turn off charger automatically.

## 3 - Device tasks
Device should connect to Wi-Fi network, wait a new request, switch on/off load and wait a new request.
### Flow
![device_tasks](./Images/device_tasks_diag_v0r0.png)
## 4 - Web app tasks
Start web page from address, then starts battery monitor, which will turn charger off. It possible to turn outlets on and off by pressing a button.
### Flow
![web_app_tasks](./Images/web_app_tasks_diag_v0r0.png)
