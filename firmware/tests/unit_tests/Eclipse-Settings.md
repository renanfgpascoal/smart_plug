# IoSE Firmware Unit Tests

## Windows

* [clone and pull IoSE Firmware repository](https://gitlab.com/iose-wifi/iose-fw/-/blob/develop/README.md#iose-firmware);
* [install Eclipse CDT](https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-ide-cc-developers);
* download and install MinGW-GCC-Toolchain: get the installer [from here](https://sourceforge.net/projects/mingw/) and follow [this tutorial](https://www.msys2.org/);
* install MinGW-GCC 32 bits [from here](https://packages.msys2.org/group/mingw-w64-i686-toolchain) or the 64-bits [from here](https://packages.msys2.org/group/mingw-w64-x86_64-toolchain) (**if using 64-bit compiler be extreamly carefull about the code you write**);
* install MinGW-GCC-GDB 32-bits [from here](https://packages.msys2.org/package/mingw-w64-i686-gdb?repo=mingw32) or the 64-bits [from here](https://packages.msys2.org/package/mingw-w64-x86_64-gdb?repo=mingw64);