/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

/*! Defines default wifi ssid */
#define WIFI_STA_DEFAULT_SSID	"house302_2.4G"

/*! Defines default password */
#define WIFI_STA_DEFAULT_PWD	"h0us3302"

/*! Defines default wifi ssid */
#define WIFI_AP_DEFAULT_SSID	"ventilar"

/*! Defines default password */
#define WIFI_AP_DEFAULT_PWD	"12345678"

#define WIFI_AP_DEFAULT_IP	"192.168.4.1"

#define WEB_APP_LOCATION 					"/"

#define MQTT_BROKER_URI				"mqtts://mqtt.flespi.io"
#define MQTT_BROKER_PORT			8883
#define MQTT_BROKER_USERNAME	"TZyJ3J8qXOZGaaezZxcFpth2QaJa3UxKIUkztgPfUFLTDOisbiVoB0ggYQgSyBq6"
#define MQTT_BROKER_PASSWORD	""
#define MQTT_CLIENT_ID				"SMART_PLUG_91E72A"
#define MQTT_CLIENT_QOS				1

/*! Defines maximum killing times of critical tasks */
#define CRITICAL_TASK_MAX_KILLS	3

/*! Defines maximum retry times of station connection */
#define STA_CONNECT_MAX_RETRIES 3

/*! Enables/disables debug prints throughout code */
#define DEBUG_PRINTS_ENABLE	1

/*! Enables/disables time showing in application log */
#define APP_LOG_SHOW_TIME		1

/*! Enable/Disable User Debug Prints */
#define DISABLE_USER_PRINTS	0

#if (DISABLE_USER_PRINTS == 0)

#else /* DISABLE_USER_PRINTS == 0 */

#endif /* DISABLE_USER_PRINTS == 0 */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */

