/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/


#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*! Enumerates IoSE Single-phase Meter Hardware Version 1 Revision 0 */
#define HW_V0R0   							0

/*! Selects IoSE Project board hardware version */
#define PROJ_HW_VERSION 						HW_V0R0

#if (PROJ_HW_VERSION == HW_V0R0)

#include "hw_v0r0.h"

#elif (PROJ_HW_VERSION == DEMO)

#include "demo.h"

#endif /* PROJ_HW_VERSION */

#ifndef DEMO_PIN
/*! Board marking: demo pin  */
#define DEMO_PIN         	0
#endif /* DEMO_PIN */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
