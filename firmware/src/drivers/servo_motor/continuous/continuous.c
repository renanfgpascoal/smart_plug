///@file continuous.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "continuous.h"
#include "application.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
pwm_hal_err_t continuous_servo_move(pwm_hal_channel_t from, uint32_t speed_pct, uint32_t direction, uint32_t time_ms) {
	uint32_t duty_us = CONTINUOUS_SERVO_CALC_CLOCKWISE_DUTY_US(speed_pct);
	if (direction == 0) {
		duty_us = CONTINUOUS_SERVO_CALC_ANTICLOCKWISE_DUTY_US(speed_pct);
	}

	if ((duty_us > CONTINUOUS_SERVO_MAX_PULSEWIDTH_US) || (duty_us < CONTINUOUS_SERVO_MIN_PULSEWIDTH_US)) {
		return PWM_HAL_INVALID_ARG;
	}

	if (pwm_hal_set_duty(from, duty_us) != PWM_HAL_OK) {
		(void) pwm_hal_set_duty(from, CONTINUOUS_SERVO_STOP_PULSEWIDTH_US);
		return PWM_HAL_FAIL;
	}

	RELATIVE_DELAY(time_ms);
	return pwm_hal_set_duty(from, CONTINUOUS_SERVO_STOP_PULSEWIDTH_US);

}
