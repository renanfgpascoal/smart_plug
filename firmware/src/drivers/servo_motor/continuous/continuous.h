/// @file continuous.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Continuous servo maximum pulsewidth in microseconds
 */
#define CONTINUOUS_SERVO_MAX_PULSEWIDTH_US	(2500)	// in microseconds

/*!
 * @brief Continuous servo minimum pulsewidth in microseconds
 */
#define CONTINUOUS_SERVO_MIN_PULSEWIDTH_US	(500)	// in microseconds

/*!
 * @brief Continuous servo stop pulsewidth in microseconds
 */
#define CONTINUOUS_SERVO_STOP_PULSEWIDTH_US	(1500)	// in microseconds

/*!
 * @brief SG90 maximum movement angle in degree
 */
#define CONTINUOUS_SERVO_MAX_SPEED					(1000)	// in microseconds

/*!
 * @brief Continuous servo duty cycle calculation for anticlockwise direction
 * @param SPEED_PCT	Rotation speed value in percentage
 */
#define CONTINUOUS_SERVO_CALC_ANTICLOCKWISE_DUTY_US(SPEED_PCT)	\
		CONTINUOUS_SERVO_STOP_PULSEWIDTH_US + (CONTINUOUS_SERVO_MAX_SPEED * (SPEED_PCT) / 100)

/*!
 * @brief Continuous servo duty cycle calculation for clockwise direction
 * @param SPEED_PCT	Rotation speed value in percentage
 */
#define CONTINUOUS_SERVO_CALC_CLOCKWISE_DUTY_US(SPEED_PCT)	\
		CONTINUOUS_SERVO_STOP_PULSEWIDTH_US - (CONTINUOUS_SERVO_MAX_SPEED * (SPEED_PCT) / 100)

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include <stdbool.h>

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "pwm_hal.h"

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Continuous servo set position in percentage function
 * @param from	PWM channel number
 * @param to		Duty cycle in percentage
 * @return True if success, otherwise false
 */
pwm_hal_err_t continuous_servo_move(pwm_hal_channel_t from, uint32_t speed_pct, uint32_t direction, uint32_t time_ms);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
