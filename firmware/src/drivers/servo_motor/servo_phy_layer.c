///@file servo_phy_layer.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "servo_phy_layer.h"
#include "continuous.h"
#include "positional.h"
#include "hw_config.h"

#include "proprietary_utils.h"
#include "proprietary_assert.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief	Servo physical layer check if PWM is running
 * @param from	PWM channel number
 * @return True if it's running, otherwise false
 */
static bool servo_phy_is_runnning(servo_phy_num_t from);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static bool servo_phy_is_runnning(servo_phy_num_t from) {
	return pwm_hal_get_period((pwm_hal_channel_t) from) == POSITIONAL_SERVO_BASE_PERIOD;
}

servo_phy_err_t servo_phy_set_pos(servo_phy_num_t from, uint32_t to) {
	CONTINUE_CONDITION(from < SERVO_NUM_MAX, SERVO_PHY_INVALID_PARAM);
	CONTINUE_CONDITION(to > 0, SERVO_PHY_INVALID_PARAM);
	CONTINUE_CONDITION(to < 100, SERVO_PHY_INVALID_PARAM);

	if (servo_phy_is_runnning(from) == false) {
		return SERVO_PHY_NOT_RUNNING;
	}

	if (positional_servo_move((pwm_hal_channel_t) from, to) == false) {
		return SERVO_PHY_FAIL;
	}

	return SERVO_PHY_OK;
}

servo_phy_err_t servo_phy_set_rot(servo_phy_num_t from,
                                  servo_phy_continuous_params_t *p_to) {
	CONTINUE_CONDITION(from < SERVO_NUM_MAX, SERVO_PHY_INVALID_PARAM);
	CONTINUE_CONDITION(p_to != NULL, SERVO_PHY_INVALID_PARAM);

	if (servo_phy_is_runnning(from) == false) {
		return SERVO_PHY_NOT_RUNNING;
	}

	if (continuous_servo_move((pwm_hal_channel_t) from,
	                          p_to->speed_pct,
	                          p_to->direction,
	                          p_to->time_ms) != PWM_HAL_OK) {
		return SERVO_PHY_FAIL;
	}

	return SERVO_PHY_OK;
}

servo_phy_err_t servo_phy_setup() {
	uint32_t pin[] = {
	PWM_0_PIN,
	PWM_1_PIN };

	uint32_t duties[] = { 0, 0, };

	float phases[] = { 0, 0, };

	pwm_hal_config_t cfg = {
	  .period_ms = POSITIONAL_SERVO_BASE_PERIOD,
	  .p_pin = pin,
	  .p_duties = duties,
	  .p_phases = phases,
	  .n_of_channels = ARRAY_SIZE(pin) };

	if (pwm_hal_init(&cfg) != PWM_HAL_OK) {
		return SERVO_PHY_FAIL;
	}

	return SERVO_PHY_OK;
}
