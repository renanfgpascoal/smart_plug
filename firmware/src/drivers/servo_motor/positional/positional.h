/// @file positional.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Positional servo duty cycle period in microseconds
 */
#define POSITIONAL_SERVO_BASE_PERIOD					20000

/*!
 * @brief Positional servo start position in degrees
 */
#define POSITIONAL_SERVO_START_POS					0

/*!
 * @brief Positional servo duty cycle mode
 */
#define POSITIONAL_SERVO_DUTY_MODE					PWM_DUTY_HIGH

/*!
 * @brief Positional servo maximum pulsewidth in microseconds
 */
#define POSITIONAL_SERVO_MAX_PULSEWIDTH_US	(2500)	// in microseconds

/*!
 * @brief Positional servo minimum pulsewidth in microseconds
 */
#define POSITIONAL_SERVO_MIN_PULSEWIDTH_US	(500)	// in microseconds

/*!
 * @brief Positional servo maximum movement angle in degree
 */
#define POSITIONAL_SERVO_MAX_ANGLE					(180)	// in degrees

/*!
 * @brief Positional servo position angle calculation
 * @param DUTY_PCT	Duty cycle value in percentage
 */
#define POSITIONAL_SERVO_CALC_ANGLE(DUTY_PCT)	\
		POSITIONAL_SERVO_MAX_ANGLE * DUTY_PCT / 100

/*!
 * @brief Positional servo duty cycle in percentage calculation
 * @param ANGLE	Movement angle in degrees
 */
#define POSITIONAL_SERVO_CALC_DUTY_PCT(ANGLE)	\
	(float)(POSITIONAL_SERVO_BASE_PERIOD * POSITIONAL_SERVO_CALC_DUTY_US((ANGLE)) / 100000000)

/*!
 * @brief Positional servo duty cycle in microseconds calculation
 * @param ANGLE	Movement angle in degrees
 */
#define POSITIONAL_SERVO_CALC_DUTY_US(ANGLE)	\
	((ANGLE)) * (POSITIONAL_SERVO_MAX_PULSEWIDTH_US - POSITIONAL_SERVO_MIN_PULSEWIDTH_US) / (POSITIONAL_SERVO_MAX_ANGLE) + POSITIONAL_SERVO_MIN_PULSEWIDTH_US

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include <stdbool.h>

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "pwm_hal.h"

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Positional servo set position in percentage function
 * @param from	PWM channel number
 * @param to		Duty cycle in percentage
 * @return True if success, otherwise false
 */
pwm_hal_err_t positional_servo_move(pwm_hal_channel_t from, uint32_t to);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
