/// @file gpio_hal_driver.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#if ESP8266 == 1

#include "gpio_esp8266_interface.h"

#endif /* ESP_PLATFORM == 1 */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */
/*!
 * @brief	GPIO Hardware Abstraction Layer (HAL) get pin level function
 * @param pin		GPIO pin number
 * @return 1 = high, or 0 = low
 */
uint32_t gpio_hal_get_pin_level(gpio_hal_pin_num_t pin);

/*!
 * @brief	GPIO Hardware Abstraction Layer (HAL) set duty cycle function
 * @param pin		GPIO pin number
 * @param level	GPIO pin level
 * @return GPIO_HAL_OK if success, otherwise error status
 */
gpio_hal_err_t gpio_hal_set_pin_level(gpio_hal_pin_num_t pin, gpio_hal_pin_level_t level);

/*!
 * @brief	GPIO Hardware Abstraction Layer (HAL) add ISR function to pin
 * @param pin			GPIO pin number
 * @param func		ISR function
 * @param p_args	Address of ISR function arguments
 * @return GPIO_HAL_OK if success, otherwise error status
 */
gpio_hal_err_t gpio_hal_isr_add(gpio_hal_pin_num_t pin, gpio_hal_isr_t func, void *p_args);

/*!
 * @brief	GPIO Hardware Abstraction Layer (HAL) add ISR setup
 * @return GPIO_HAL_OK if success, otherwise error status
 */
gpio_hal_err_t gpio_hal_isr_setup(void);

/*!
 * @brief	GPIO Hardware Abstraction Layer (HAL) setup function
 * @param p_config	Address of structure holding GPIO pin configuration data
 * @return GPIO_HAL_OK if success, otherwise error status
 */
gpio_hal_err_t gpio_hal_setup(gpio_hal_config_t *p_config);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
