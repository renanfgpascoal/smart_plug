///@file gpio_hal_driver.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "gpio_hal_driver.h"
#include "proprietary_assert.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

uint32_t gpio_hal_get_pin_level(gpio_hal_pin_num_t pin) {
	return GPIO_HAL_GET_LEVEL(pin);
}

gpio_hal_err_t gpio_hal_set_pin_level(gpio_hal_pin_num_t pin, gpio_hal_pin_level_t level) {
	CONTINUE_CONDITION(level < GPIO_PIN_LEVEL_MAX, GPIO_HAL_INVALID_ARG);

	return GPIO_HAL_SET_LEVEL(pin, level);
}

gpio_hal_err_t gpio_hal_isr_add(gpio_hal_pin_num_t pin, gpio_hal_isr_t func, void *p_args) {
	CONTINUE_CONDITION(func != NULL, GPIO_HAL_INVALID_ARG);

	return GPIO_HAL_ISR_ADD(pin, func, p_args);
}

gpio_hal_err_t gpio_hal_isr_setup(void) {
	return GPIO_HAL_ISR_SETUP();
}

gpio_hal_err_t gpio_hal_setup(gpio_hal_config_t *p_config) {
	CONTINUE_CONDITION(p_config != NULL, GPIO_HAL_INVALID_ARG);
	CONTINUE_CONDITION((gpio_hal_modes_t)p_config->mode < GPIO_MODES_MAX, GPIO_HAL_INVALID_ARG);
	CONTINUE_CONDITION((gpio_hal_int_types_t)p_config->intr_type < GPIO_INT_TYPES_MAX, GPIO_HAL_INVALID_ARG);

	return GPIO_HAL_SETUP(p_config);
}
