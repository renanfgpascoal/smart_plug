/// @file pwm_hal.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#if ESP8266 == 1

#include "pwm_esp8266_interface.h"

#endif /* ESP_PLATFORM == 1 */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */
/*!
 * @brief	PWM Hardware Abstraction Layer (HAL) get duty cycle frequency function
 * @param channel		PWM channel number
 * @return Duty cycle frequency in hertz (Hz)
 */
uint32_t pwm_hal_get_period(pwm_hal_channel_t channel);

/*!
 * @brief	PWM Hardware Abstraction Layer (HAL) set duty cycle function
 * @param channel		PWM channel number
 * @param duty_us		Duty cycle values in microseconds (us)
 * @return PWM_HAL_OK if success, otherwise error status
 */
pwm_hal_err_t pwm_hal_set_duty(pwm_hal_channel_t channel, uint32_t duty_us);

/*!
 * @brief	PWM Hardware Abstraction Layer (HAL) initialization function
 * @param p_config	Address of structure holding PWM configuration data
 * @return PWM_HAL_OK if success, otherwise error status
 */
pwm_hal_err_t pwm_hal_init(pwm_hal_config_t *p_config);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
