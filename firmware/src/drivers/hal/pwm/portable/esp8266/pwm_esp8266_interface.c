///@file

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "pwm_esp8266_interface.h"
#include "proprietary_utils.h"
#include "proprietary_assert.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

uint32_t pwm_esp8266_get_period(void) {
	uint32_t period_us = 0;
	if (pwm_get_period(&period_us) != ESP_OK) {
		return 0xffffffff;
	}

	return period_us;
}

pwm_hal_err_t pwm_esp8266_set_duty(pwm_hal_channel_t channel, uint32_t duty_us) {
  if (pwm_stop(0) != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

	if (pwm_set_duty((uint8_t)channel, duty_us) != ESP_OK) {
		return PWM_HAL_FAIL;
	}

  if (pwm_start() != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

	return PWM_HAL_OK;
}

pwm_hal_err_t pwm_esp8266_init(pwm_hal_config_t *p_config) {
	DEVELOPMENT_ASSERT(p_config != NULL, ESP_ERR_INVALID_ARG);

	if (pwm_init(p_config->period_ms,
	             p_config->p_duties,
	             p_config->n_of_channels,
	             p_config->p_pin) != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

  if (pwm_set_phases(p_config->p_phases) != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

  if (pwm_start() != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

  return PWM_HAL_OK;
}

pwm_hal_err_t pwm_esp8266_deinit(void) {
	if (pwm_deinit() != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

  if (pwm_stop(0) != ESP_OK) {
  	return PWM_HAL_FAIL;
  }

  return PWM_HAL_OK;
}
