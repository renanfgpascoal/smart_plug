/// @file pwm_esp32_interface.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief	Encode PWM channel number
 * @param TIMER			PWM timer
 * @param OPERATOR	PWM operator
 */
#define ENCODE_CHANNEL(TIMER, OPERATOR)	\
	((TIMER) << 2) | ((OPERATOR) << 0)

/*!
 * @brief	Decode PWM operator
 * @param CHANNEL		PWM channel number
 */
#define DECODE_OPERATOR(CHANNEL)	\
	(0x3 & (CHANNEL))

/*!
 * @brief	Decode PWM timer
 * @param CHANNEL		PWM channel number
 */
#define DECODE_TIMER(CHANNEL)	\
	((CHANNEL) >> 2)

/*!
 * @brief Interface for PWM Hardware Abstraction Layer (HAL) pin configuration
 * @param PWM_PIN_1		Defined pin as PWM channel 1
 * @param PWM_PIN_2		Defined pin as PWM channel 2
 * @param PWM_PIN_3		Defined pin as PWM channel 3
 * @param PWM_PIN_4		Defined pin as PWM channel 4
 */
#define PWM_HAL_PIN_CONFIG(PWM_PIN_0, PWM_PIN_1, PWM_PIN_2, PWM_PIN_3)	\
	{	\
		.mcpwm0a_out_num = (PWM_PIN_0),	\
		.mcpwm0b_out_num = (PWM_PIN_1),	\
		.mcpwm1a_out_num = (PWM_PIN_2),	\
		.mcpwm1b_out_num = (PWM_PIN_3)	\
	}

/*!
 * @brief Interface for PWM Hardware Abstraction Layer (HAL) configuration
 * @param FREQ				Servo motor frequency for duty cycle base
 * @param INIT_DUTY		Initial duty cycle value in percentage (%)
 * @param DUTY_MODE		Duty cycle operation mode
 */
#define PWM_HAL_CONFIG(FREQ, INIT_DUTY, DUTY_MODE)	\
	{	\
		.frequency = (FREQ),	\
		.cmpr_a = (INIT_DUTY),	\
		.cmpr_b = (INIT_DUTY),	\
		.counter_mode = MCPWM_UP_COUNTER,	\
		.duty_mode = (DUTY_MODE),	\
	}

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "driver/mcpwm.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Interface for PWM Hardware Abstraction Layer (HAL) pin configuration structure */
typedef mcpwm_pin_config_t pwm_hal_pin_config_t;

/*! Interface for PWM Hardware Abstraction Layer (HAL) configuration structure */
typedef mcpwm_config_t pwm_hal_config_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) error types */
typedef enum pwm_hal_err_t {
	/*! Success */
	PWM_HAL_OK = ESP_OK,
	/*! Failure */
	PWM_HAL_FAIL = ESP_FAIL,
	/*! Invalid argument */
	PWM_HAL_INVALID_ARG = ESP_ERR_INVALID_ARG,
	/*! Maximum error types */
	PWM_HAL_ERR_MAX
} pwm_hal_err_t;

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) duty cycle operation modes */
typedef enum pwm_hal_duty_mode_t {
	/*! Active high duty */
	PWM_DUTY_HIGH = MCPWM_DUTY_MODE_0,
	/*! Active low duty */
	PWM_DUTY_LOW = MCPWM_DUTY_MODE_1,
	/*! Maximum duty cycle modes */
	PWM_DUTY_MODE_MAX
} pwm_hal_duty_mode_t;

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) timers for PWM generation */
typedef enum pwm_hal_timers_t {
	/*! Timer 0 PWM generator*/
	PWM_TIMER_0 = MCPWM_TIMER_0,
	/*! Timer 1 PWM generator*/
	PWM_TIMER_1 = MCPWM_TIMER_1,
	/*! Timer 2 PWM generator*/
	PWM_TIMER_2 = MCPWM_TIMER_2,
	/*! Maximum PWM timers */
	PWM_TIMER_MAX
} pwm_hal_timers_t;

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) duty cycle operation modes */
typedef enum pwm_hal_channel_t {
	/*! PWM channel 0 */
	PWM_CHANNEL_0 = ENCODE_CHANNEL(MCPWM_TIMER_0, MCPWM_OPR_A),
	/*! PWM channel 1 */
	PWM_CHANNEL_1 = ENCODE_CHANNEL(MCPWM_TIMER_0, MCPWM_OPR_B),
	/*! PWM channel 2 */
	PWM_CHANNEL_2 = ENCODE_CHANNEL(MCPWM_TIMER_1, MCPWM_OPR_A),
	/*! PWM channel 3 */
	PWM_CHANNEL_3 = ENCODE_CHANNEL(MCPWM_TIMER_1, MCPWM_OPR_B),
	/*! Maximum PWM channels */
	PWM_CHANNEL_MAX
} pwm_hal_channel_t;

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) get frequency function
 * @param CHANNEL		PWM channel number
 * @return Current PWM base frequency in hertz
 */
#define PWM_HAL_GET_FREQ(CHANNEL)	\
	mcpwm_get_frequency(MCPWM_UNIT_0, DECODE_TIMER((CHANNEL)))

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) set duty cycle function
 * @param CHANNEL		PWM channel number
 * @param DUTY_US		Duty cycle values in microseconds (us)
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_SET_DUTY(CHANNEL, DUTY_US)	\
	(pwm_hal_err_t)mcpwm_set_duty_in_us(MCPWM_UNIT_0, \
	                                    DECODE_TIMER(CHANNEL), \
	                                    DECODE_OPERATOR((CHANNEL)), \
	                                    (DUTY_US))

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) setup function
 * @param P_CONFIG	Address of structure holding PWM pin configuration data
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_PIN_SETUP(P_CONFIG)	\
	(pwm_hal_err_t)mcpwm_set_pin(MCPWM_UNIT_0, \
	                             (const mcpwm_pin_config_t *)(P_CONFIG))

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) initialization function
 * @param P_CONFIG	Address of structure holding PWM configuration data
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_INIT(TIMER, P_CONFIG)	\
	(pwm_hal_err_t)mcpwm_init(MCPWM_UNIT_0, \
	                          (TIMER), \
	                          (mcpwm_config_t *)(P_CONFIG))

#ifdef __cplusplus
}
#endif /*  __cplusplus */
