///@file pwm_hal.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "pwm_hal.h"
#include "proprietary_assert.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

uint32_t pwm_hal_get_period(pwm_hal_channel_t channel) {
	CONTINUE_CONDITION(channel < PWM_CHANNEL_MAX, PWM_HAL_INVALID_ARG);

	return PWM_HAL_GET_PERIOD(channel);
}

pwm_hal_err_t pwm_hal_set_duty(pwm_hal_channel_t channel, uint32_t duty_us) {
	CONTINUE_CONDITION(channel < PWM_CHANNEL_MAX, PWM_HAL_INVALID_ARG);

	return PWM_HAL_SET_DUTY(channel, duty_us);
}

pwm_hal_err_t pwm_hal_init(pwm_hal_config_t *p_config) {
	CONTINUE_CONDITION(p_config != NULL, PWM_HAL_INVALID_ARG);

	return PWM_HAL_INIT(p_config);
}
