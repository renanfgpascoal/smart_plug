///@file button_phy_layer.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <stdbool.h>

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "fan_control_application.h"
#include "application.h"

#include "button_phy_layer.h"
#include "hw_config.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */
/* Post-include functioning defines start */

#define BUTON_PHY_GET_SPD_STATUS()	\
	gpio_hal_get_pin_level(BUTTON_0_PIN) << 0

#define BUTON_PHY_GET_OSC_STATUS()	\
	gpio_hal_get_pin_level(BUTTON_1_PIN) << 1

/* Post-include functioning defines end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum button_phy_status_t {
	BUTTON_PHY_OSC_PRESSED = (1 << 0),
	BUTTON_PHY_SPD_PRESSED = (1 << 1)
} button_phy_status_t;

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static TimerHandle_t button_phy_spd_select_timer = NULL;
static TimerHandle_t button_phy_deb_timer = NULL;
static uint32_t button_phy_spd_press_times = 0;
static uint32_t button_phy_spd_hold_times = 0;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

IRAM_ATTR static void button_phy_spd_select_timer_cb(TimerHandle_t p_timer_handle) {
	if (BUTON_PHY_GET_SPD_STATUS() == 0) {
		button_phy_spd_hold_times++;
		if (button_phy_spd_hold_times == 2) {
			fan_control_app_req_from_isr(FAN_CONTROL_SPD_OFF_REQ);
			button_phy_spd_hold_times = 0;
			button_phy_spd_press_times = 0;
			return;
		}

		TIMER_OS_RESET_ISR(button_phy_spd_select_timer);
		return;
	}

	switch (button_phy_spd_press_times) {
		case 1: {
			fan_control_app_req_from_isr(FAN_CONTROL_SPD_MIN_REQ);
			break;
		}
		case 2: {
			fan_control_app_req_from_isr(FAN_CONTROL_SPD_MED_REQ);
			break;
		}
		case 3: {
			fan_control_app_req_from_isr(FAN_CONTROL_SPD_MAX_REQ);
			break;
		}
		default: {
			break;
		}
	}

	button_phy_spd_press_times = 0;
	return;
}

IRAM_ATTR static void button_phy_debounce_timer_cb(TimerHandle_t p_timer_handle) {
	uint32_t btn_pin_levels = (BUTON_PHY_GET_SPD_STATUS()) | (BUTON_PHY_GET_OSC_STATUS());

	/* Blocking pin debouncing */
	if (btn_pin_levels == 0x03) {
		return;
	}

	switch (btn_pin_levels) {
		case BUTTON_PHY_SPD_PRESSED: {
			button_phy_spd_press_times++;
			TIMER_OS_RESET_ISR(button_phy_spd_select_timer);
			break;
		}
		case BUTTON_PHY_OSC_PRESSED: {
			fan_control_app_req_from_isr(FAN_CONTROL_OSC_TOGGLE_REQ);
			break;
		}
		default:
			break;
	}

	return;
}


IRAM_ATTR static void button_phy_button_0_isr_handler(void *p_arg) {
	TIMER_OS_RESET_ISR(p_arg);
	YIELD_ISR();

	return;
}

IRAM_ATTR static void button_phy_button_1_isr_handler(void *p_arg) {
	TIMER_OS_RESET_ISR(p_arg);
	YIELD_ISR();

	return;
}

button_phy_err_t button_phy_setup(void) {
	gpio_hal_config_t cfg = GPIO_HAL_CONFIG(GPIO_HAL_SEL(BUTTON_0_PIN) | (GPIO_HAL_SEL(BUTTON_1_PIN)),
	                                        GPIO_HAL_MODE_INPUT,
	                                        true,
	                                        false,
	                                        GPIO_INT_NEGEDGE);
	if (gpio_hal_setup(&cfg) != GPIO_HAL_OK) {
		return BUTTON_PHY_FAIL;
	}

	if (gpio_hal_isr_setup() != GPIO_HAL_OK) {
		return BUTTON_PHY_FAIL;
	}

	button_phy_spd_select_timer = TIMER_OS_CREATE("speed_select_timer",
																								800,
																								false,
																								&button_phy_spd_select_timer_cb);

	button_phy_deb_timer = TIMER_OS_CREATE("Debounce_timer",
	                                       50,
	                                       false,
	                                       &button_phy_debounce_timer_cb);

	if (gpio_hal_isr_add(BUTTON_0_PIN,
	                     &button_phy_button_0_isr_handler,
	                     button_phy_deb_timer) != GPIO_HAL_OK) {
		return BUTTON_PHY_FAIL;
	}

	if (gpio_hal_isr_add(BUTTON_1_PIN,
	                     &button_phy_button_1_isr_handler,
	                     button_phy_deb_timer) != GPIO_HAL_OK) {
		return BUTTON_PHY_FAIL;
	}

	return BUTTON_PHY_OK;
}
