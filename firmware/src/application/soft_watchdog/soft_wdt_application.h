/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Sets kick flag from specific application
 * @param from Application index
 */
void soft_wdt_app_kick(app_index_t from);

/*!
 * @brief Configures Application
 * @param p_configs Address of structure holding input configurations: list of queues to exchange data and list of tasks to notify
 */
void soft_wdt_app_cfg(app_info_t *p_from, uint32_t wake_time);

/*!
 * @brief Generates Application main task
 * @param task_prio Priority number for main task of this application
 */
void soft_wdt_app_run(void);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
