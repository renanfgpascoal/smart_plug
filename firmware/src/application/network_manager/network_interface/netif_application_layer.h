/// @file netif_application_layer.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"
#include "hw_config.h"

#if IS_WIFI_SUPPORTED(HW_VERSION)

#include "wifi_application_layer.h"

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef struct netif_wifi_config_t {

#if IS_WIFI_SUPPORTED(HW_VERSION)

	wifi_app_config_t config;
	wifi_app_layer_mode_t mode;

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */

} netif_wifi_config_t;

typedef struct netif_config_t {
	netif_wifi_config_t wifi_config;
} netif_config_t;

typedef enum netif_app_interfaces_t {
	NETIF_WIFI,
} netif_app_interfaces_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Checks if network interface is connected
 * @param timeout_ms Timeout value in milliseconds
 * @return True if connected, false otherwise
 */
bool netif_app_layer_is_connected(uint32_t timeout_ms);
bool netif_app_layer_connect(netif_app_interfaces_t interface);

/*!
 * @brief Initializes network interface application layer
 * @param p_from Address of structure holding network interface configuration parameters
 * @return True if success, false otherwise
 */
bool netif_app_layer_init(netif_config_t *p_from);

/*!
 * @brief Deinitializes network interface application layer
 * @return True if success, false otherwise
 */
bool netif_app_layer_deinit();

#ifdef __cplusplus
}
#endif /*  __cplusplus */
