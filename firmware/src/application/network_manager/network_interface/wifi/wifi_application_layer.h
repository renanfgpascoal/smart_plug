/// @file wifi_application_layer.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"
#include "wifi_portable_layer.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

/*! Enumeration of Wifi modes */
typedef enum wifi_app_layer_mode_t{
	/*! WiFi null mode */
  WIFI_APP_LAYER_MODE_NULL = 0,
	/*! WiFi station mode */
	WIFI_APP_LAYER_MODE_STA,
	/*! WiFi soft-AP mode */
	WIFI_APP_LAYER_MODE_AP,
	/*! WiFi station/soft-AP mode */
	WIFI_APP_LAYER_MODE_APSTA,
	/*! WiFi maximum mode counting */
	WIFI_APP_LAYER_MODE_MAX,
}wifi_app_layer_mode_t;

/*! Enumeration of Wifi interfaces */
typedef enum wifi_app_layer_interface_t{
	/*! WiFi station interface */
	WIFI_APP_LAYER_IF_STA = 0,
	/*! WiFi soft-AP interface */
	WIFI_APP_LAYER_IF_AP,
	/*! Maximum WiFi interfaces */
	WIFI_APP_LAYER_MAX_IF,
}wifi_app_layer_interface_t;

#else

/*! Enumeration of Wifi interfaces */
typedef enum wifi_app_layer_mode_t {
	/*! WiFi null mode */
	WIFI_APP_LAYER_MODE_NULL = WIFI_MODE_NULL,
	/*! WiFi station mode */
	WIFI_APP_LAYER_MODE_STA = WIFI_MODE_STA,
	/*! WiFi soft-AP mode */
	WIFI_APP_LAYER_MODE_AP = WIFI_MODE_AP,
	/*! WiFi station/soft-AP mode */
	WIFI_APP_LAYER_MODE_APSTA = WIFI_MODE_APSTA,
}wifi_app_layer_mode_t;

/*! Enumeration of Wifi interfaces */
typedef enum wifi_app_layer_interface_t {
	/*! WiFi station interface */
	WIFI_APP_LAYER_IF_STA = ESP_IF_WIFI_STA,
	/*! WiFi soft-AP interface */
	WIFI_APP_LAYER_IF_AP = ESP_IF_WIFI_AP,
	/*! Maximum WiFi interfaces */
	WIFI_APP_LAYER_MAX_IF = ESP_IF_MAX,
}wifi_app_layer_interface_t;

#endif /* APP_TRACE_POSIX */

typedef struct wifi_app_config_t {
	wifi_config_t *p_sta;
	wifi_config_t *p_ap;
} wifi_app_config_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

char *wifi_app_layer_get_sta_ip_addr(uint32_t timeout_ms);

char *wifi_app_layer_get_sta_ip_info(uint32_t timeout_ms);

char *wifi_app_layer_get_scan_list(uint32_t timeout_ms);

bool wifi_app_layer_start_scan_ap(uint32_t timeout_ms);

/*!
 * @brief Checks if wifi is connected
 * @param timeout_ms Timeout value in milliseconds
 * @return True if connected, false otherwise
 */
bool wifi_app_layer_is_connected(uint32_t timeout_ms);

bool wifi_app_connect();

bool wifi_app_disconnect();

/*!
 * @brief Initializes wifi application layer
 * @param p_from Address of structure holding wifi configuration parameters
 * @param to Specify which wifi interface to initialize
 * @return True if success, false otherwise
 */
bool wifi_app_layer_init(wifi_app_config_t *p_from, wifi_app_layer_mode_t to);

/*!
 * @brief Deinitializes wifi application layer
 * @return True if success, false otherwise
 */
bool wifi_app_layer_deinit();

#ifdef __cplusplus
}
#endif /*  __cplusplus */
