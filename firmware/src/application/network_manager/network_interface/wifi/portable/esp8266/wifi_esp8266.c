///@file wifi_esp8266.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "lwip/err.h"
#include "lwip/sys.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "wifi_portable_layer.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

app_err_t wifi_port_connect(void) {
	if (esp_wifi_connect() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_disconnect(void) {
	if (esp_wifi_disconnect() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

void wifi_port_netif_init(void) {
	tcpip_adapter_init();

	return;
}

app_err_t wifi_port_init(const wifi_init_config_t *p_config) {
	if (esp_wifi_init(p_config) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_deinit(void) {
	if (esp_wifi_deinit() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_set_mode(wifi_mode_t mode) {
	if (esp_wifi_set_mode(mode) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_set_config(wifi_interface_t interface, wifi_config_t *p_config) {
	if (esp_wifi_set_config(interface, p_config) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_start(void) {
	if (esp_wifi_start() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t wifi_port_stop(void) {
	if (esp_wifi_stop() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}


char *wifi_port_ip_to_str(const ip4_addr_t *addr) {
	return ip4addr_ntoa(addr);
}

app_err_t wifi_port_scan_get_ap_records(uint16_t *number, wifi_ap_record_t *ap_records) {
	if (esp_wifi_scan_get_ap_records(number, ap_records) != ESP_OK) {
		return APP_FAIL;
	}
	return APP_OK;
}

app_err_t wifi_port_scan_start(const wifi_scan_config_t *config, bool block) {
	if (esp_wifi_scan_start(config, block) != ESP_OK) {
		return APP_FAIL;
	}
	return APP_OK;
}
