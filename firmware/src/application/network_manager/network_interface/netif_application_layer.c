///@file netif_application_layer.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "netif_application_layer.h"
#include "proprietary_utils.h"

#if IS_WIFI_SUPPORTED(HW_VERSION)

#include "wifi_application_layer.h"

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */


/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

bool netif_app_layer_is_connected(uint32_t timeout_ms) {
#if IS_WIFI_SUPPORTED(HW_VERSION)

	return wifi_app_layer_is_connected(timeout_ms);

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */
}

bool netif_app_layer_connect(netif_app_interfaces_t interface) {
	if (interface == NETIF_WIFI) {
		return wifi_app_connect();
	}

	return false;
}

bool netif_app_layer_init(netif_config_t *p_from) {
	DEVELOPMENT_ASSERT(p_from != NULL, false);

#if IS_WIFI_SUPPORTED(HW_VERSION)

	return wifi_app_layer_init(&p_from->wifi_config.config,
	                           p_from->wifi_config.mode);

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */
}

bool netif_app_layer_deinit() {
#if IS_WIFI_SUPPORTED(HW_VERSION)

	return wifi_app_layer_deinit();

#endif /* IS_WIFI_SUPPORTED(HW_VERSION) */
}

