///@file mqtt_esp826.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "esp_tls.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "mqtt_portable_layer.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

mqtt_port_handler_t p_func_handler = NULL;
static uint32_t mqtt_port_qos = 0;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void mqtt_port_event_handler(void *handler_args,
                                    esp_event_base_t base,
                                    int32_t event_id,
                                    void *event_data) {
	p_func_handler(event_data);
}

int32_t mqtt_port_publish(mqtt_port_client_handle_t client,
                          const char *p_topic,
                          const char *p_data) {
	return esp_mqtt_client_publish(client, p_topic, p_data, 0, mqtt_port_qos, 0);
}

int32_t mqtt_port_subscribe(mqtt_port_client_handle_t client,
                            const char *p_topic) {
	return esp_mqtt_client_subscribe(client, p_topic, mqtt_port_qos);
}

#include "application.h"

app_err_t mqtt_port_setup(mqtt_port_config_t *p_config) {
	const esp_mqtt_client_config_t cfg = {
	  .uri = p_config->p_uri,
	  .port = p_config->port,
	  .cert_pem = (const char*) p_config->p_tls_cert,
	  .client_id = p_config->p_client_id,
	  .username = p_config->p_username,
	  .password = p_config->p_password
	};

	p_func_handler = p_config->p_func_handler;
	mqtt_port_qos = p_config->qos;

	esp_mqtt_client_handle_t client = esp_mqtt_client_init(&cfg);

	uint32_t err = esp_mqtt_client_register_event(client,
	                                     ESP_EVENT_ANY_ID,
	                                     mqtt_port_event_handler,
	                                     client);
	APP_LOG(LOG_WARN, FMT_ARGS("st: 0x%04X, client: %p", err, client));
	if (err != ESP_OK) {
		return APP_FAIL;
	}

	if (esp_mqtt_client_start(client) != ESP_OK) {
		return APP_FAIL;
	}

	*p_config->p_client = client;

	return APP_OK;
}
