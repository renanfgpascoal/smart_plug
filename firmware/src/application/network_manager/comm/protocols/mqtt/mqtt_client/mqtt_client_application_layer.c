///@file netif_application_layer.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "mqtt_client_application_layer.h"
#include "fan_control_application.h"
#include "mqtt_portable_layer.h"
#include "proprietary_utils.h"
#include "fw_config.h"
#include "decode.h"


#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static app_err_t mqtt_client_app_layer_event_handler(mqtt_port_event_handle_t from);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

extern const char mqtt_public_pem_start[]   asm("_binary_mqtt_flespi_io_pem_start");
extern const char mqtt_public_pem_end[]   asm("_binary_mqtt_flespi_io_pem_end");
mqtt_port_client_handle_t client = NULL;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static app_err_t mqtt_client_app_layer_event_handler(mqtt_port_event_handle_t from) {
	mqtt_port_client_handle_t client = from->client;

	switch (from->event_id) {
		case MQTT_PORT_CONNECTED: {
			 APP_LOG(LOG_WARN, FMT_NONE("MQTT_PORT_CONNECTED"));
			 (void)mqtt_port_subscribe(client, "/fan_assist/operations");

			 break;
		}
		case MQTT_PORT_DISCONNECTED: {
			 APP_LOG(LOG_WARN, FMT_NONE("MQTT_PORT_DISCONNECTED"));
			 break;
		}
		case MQTT_PORT_SUBSCRIBED: {
			 APP_LOG(LOG_WARN, FMT_ARGS("MQTT_PORT_SUBSCRIBED, msg_id=%d", from->msg_id));
			 break;
		}
		case MQTT_PORT_PUBLISHED: {
			 APP_LOG(LOG_WARN, FMT_ARGS("MQTT_PORT_PUBLISHED, msg_id=%d", from->msg_id));
			 break;
		}
		case MQTT_PORT_RX_DATA: {
			 APP_LOG(LOG_WARN, FMT_NONE("MQTT_PORT_DATA"));
			 APP_LOG(LOG_WARN, FMT_ARGS("TOPIC=%.*s\r\n", from->topic_len, from->topic));
			 APP_LOG(LOG_WARN, FMT_ARGS("DATA=%.*s\r\n", from->data_len, from->data));

			 uint32_t op = decode_msg_op(from->data);
			 APP_LOG(LOG_WARN, FMT_ARGS("OP=%u\n", op));
			 fan_control_app_req(op);
			 break;
		}
		case MQTT_PORT_ERROR: {
			 APP_LOG(LOG_ERROR, FMT_NONE("MQTT_PORT_ERROR"));
			 if (from->error_handle->error_type == MQTT_ERROR_TYPE_ESP_TLS) {
					 APP_LOG(LOG_ERROR, FMT_ARGS("Last error code reported from esp-tls: 0x%x", from->error_handle->esp_tls_last_esp_err));
					 APP_LOG(LOG_ERROR, FMT_ARGS("Last tls stack error number: 0x%x", from->error_handle->esp_tls_stack_err));
			 } else if (from->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED) {
					 APP_LOG(LOG_ERROR, FMT_ARGS("Connection refused error: 0x%x", from->error_handle->connect_return_code));
			 } else {
				 APP_LOG(LOG_ERROR, FMT_ARGS("Unknown error type: 0x%x", from->error_handle->error_type));
			 }
			 break;
		}
		default: {
			 APP_LOG(LOG_ERROR, FMT_ARGS("Other event id:%d", from->event_id));
			 break;
		}
	}

   return ESP_OK;
}

int32_t mqtt_client_app_layer_publish(const char *p_from, const char *p_to) {
	return mqtt_port_publish(client, p_from, p_to);
}

int32_t mqtt_client_app_layer_subscribe(const char *p_to) {
	return mqtt_port_subscribe(client, p_to);
}

bool mqtt_client_app_layer_init(void) {
	mqtt_port_config_t cfg = {
		.p_uri = MQTT_BROKER_URI,
		.port = MQTT_BROKER_PORT,
		.p_tls_cert = mqtt_public_pem_start,
		.p_client_id = MQTT_CLIENT_ID,
		.p_username = MQTT_BROKER_USERNAME,
		.p_password = MQTT_BROKER_PASSWORD,
		.p_client = &client,
		.p_func_handler = mqtt_client_app_layer_event_handler,
		.qos = MQTT_CLIENT_QOS
	};

	if (mqtt_port_setup(&cfg) != APP_OK) {
		return false;
	}

	return true;
}

bool mqtt_client_app_layer_deinit() {
	return true;
}

