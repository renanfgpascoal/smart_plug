/// @file wifi_portable_layer.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#if ESP_PLATFORM == 1

#include "esp_event.h"
#include "mqtt_client.h"
#include "app_err.h"

#endif

#ifdef APP_TRACE_POSIX

#endif /* APP_TRACE_POSIX */

/* Module functioning includes end */

/*******************************************************************
 * POST INCLUDES EXTERNED DEFINES
 *******************************************************************/

#ifdef APP_TRACE_POSIX

#else /* APP_TRACE_POSIX */

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

#else /* APP_TRACE_POSIX */

typedef esp_mqtt_client_handle_t mqtt_port_client_handle_t;

typedef esp_mqtt_event_handle_t mqtt_port_event_handle_t;

typedef app_err_t (*mqtt_port_handler_t)(mqtt_port_event_handle_t);

typedef struct mqtt_port_config_t {
	const char *p_uri;
	uint32_t port;
	const char *p_tls_cert;
	const char *p_client_id;
	const char *p_username;
	const char *p_password;
	mqtt_port_client_handle_t *p_client;
	mqtt_port_handler_t p_func_handler;
	uint32_t qos;
} mqtt_port_config_t;

typedef enum mqtt_port_events_t {
	MQTT_PORT_CONNECTED = MQTT_EVENT_CONNECTED,
	MQTT_PORT_DISCONNECTED = MQTT_EVENT_DISCONNECTED,
	MQTT_PORT_SUBSCRIBED = MQTT_EVENT_SUBSCRIBED,
	MQTT_PORT_PUBLISHED = MQTT_EVENT_PUBLISHED,
	MQTT_PORT_RX_DATA = MQTT_EVENT_DATA,
	MQTT_PORT_ERROR = MQTT_EVENT_ERROR,
} mqtt_port_events_t;

typedef enum mqtt_port_error_t {
	MQTT_PORT_ERR_OK = MQTT_ERROR_TYPE_NONE,
	MQTT_PORT_ERR_TLS = MQTT_ERROR_TYPE_ESP_TLS,
	MQTT_PORT_ERR_CONN_REFUSED = MQTT_ERROR_TYPE_CONNECTION_REFUSED,
} mqtt_port_error_t;

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX


#else

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

int32_t mqtt_port_publish(mqtt_port_client_handle_t client,
                          const char *p_topic,
                          const char *p_data);
int32_t mqtt_port_subscribe(mqtt_port_client_handle_t client,
                            const char *p_topic);
app_err_t mqtt_port_setup(mqtt_port_config_t *p_config);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
