///@file network_mgr_application.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

#include <string.h>

#endif /* APP_TRACE_POSIX */

/* System functioning includes end */

/* Module functioning includes start */

#include "http_server.h"
#include "http_methods/http_methods.h"
#include "portable/httpd_portable_layer.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Application main RTOS task
 * @param p_arg Address of arguments passed on task execution
 */
static char* http_server_generate_url(const char *page);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
static httpd_port_handle_t httpd_handle = NULL;

static const httpd_port_uri_t http_server_get_index = {
  .uri = "/",
  .method = HTTP_GET,
  .handler = http_method_get_index,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_get_css = {
  .uri = "/style.css",
  .method = HTTP_GET,
  .handler = http_method_get_css,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_get_js = {
  .uri = "/code.js",
  .method = HTTP_GET,
  .handler = http_method_get_js,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_get_ap = {
  .uri = "/ap.json",
  .method = HTTP_GET,
  .handler = http_method_get_ap,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_get_status = {
  .uri = "/status.json",
  .method = HTTP_GET,
  .handler = http_method_get_status,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_post_connect = {
  .uri = "/connect.json",
  .method = HTTP_POST,
  .handler = http_method_post_connect,
  .user_ctx = NULL, };

static const httpd_port_uri_t http_server_delete_connect = {
  .uri = "/connect.json",
  .method = HTTP_DELETE,
  .handler = http_method_delete_connect,
  .user_ctx = NULL, };

/* strings holding the URLs of the wifi manager */
char *http_root_url = NULL;
char *http_redirect_url = NULL;
char *http_js_url = NULL;
char *http_css_url = NULL;
char *http_connect_url = NULL;
char *http_ap_url = NULL;
char *http_status_url = NULL;
//static char* http_mac_url = NULL;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static char* http_server_generate_url(const char *page) {
	char *ret;

	int root_len = strlen(WEB_APP_LOCATION);
	const size_t url_sz = sizeof(char) * ((root_len + 1) + (strlen(page) + 1));

	ret = malloc(url_sz);
	memset(ret, 0x00, url_sz);
	strcpy(ret, WEB_APP_LOCATION);
	ret = strcat(ret, page);

	return ret;
}

void http_server_stop() {
	if (httpd_handle != NULL) {
		/* dealloc URLs */
		if (http_root_url) {
			free(http_root_url);
			http_root_url = NULL;
		}
		if (http_redirect_url) {
			free(http_redirect_url);
			http_redirect_url = NULL;
		}
		if (http_js_url) {
			free(http_js_url);
			http_js_url = NULL;
		}
		if (http_css_url) {
			free(http_css_url);
			http_css_url = NULL;
		}
		if (http_connect_url) {
			free(http_connect_url);
			http_connect_url = NULL;
		}
		if (http_ap_url) {
			free(http_ap_url);
			http_ap_url = NULL;
		}
		if (http_status_url) {
			free(http_status_url);
			http_status_url = NULL;
		}

		/* stop server */
		httpd_port_stop(httpd_handle);
		httpd_handle = NULL;
	}
	return;
}

void http_server_start(bool lru_purge_enable) {
	app_err_t err;
	if (httpd_handle == NULL) {

		httpd_port_config_t config = HTTPD_PORT_DEFAULT_CONFIG();

//		/* this is an important option that isn't set up by default.
//		 * We could register all URLs one by one, but this would not work while the fake DNS is active */
//		config.uri_match_fn = httpd_port_uri_match_wildcard;
		config.lru_purge_enable = lru_purge_enable;

		/* generate the URLs */
		if (http_root_url == NULL) {
			int root_len = strlen(WEB_APP_LOCATION);

			/* all the pages */
			const char page_js[] = "code.js";
			const char page_css[] = "style.css";
			const char page_connect[] = "connect.json";
			const char page_ap[] = "ap.json";
			const char page_status[] = "status.json";
//			const char page_mac[] = "mac.json";

			/* root url, eg "/"   */
			const size_t http_root_url_sz = sizeof(char) * (root_len + 1);
			http_root_url = malloc(http_root_url_sz);
			memset(http_root_url, 0x00, http_root_url_sz);
			strcpy(http_root_url, WEB_APP_LOCATION);

			/* redirect url */
			size_t redirect_sz = 22 + root_len + 1; /* strlen(http://255.255.255.255) + strlen("/") + 1 for \0 */
			http_redirect_url = malloc(sizeof(char) * redirect_sz);
			*http_redirect_url = '\0';

			if (root_len == 1) {
				snprintf(http_redirect_url,
				         redirect_sz,
				         "http://%s",
				         WIFI_AP_DEFAULT_IP);
			} else {
				snprintf(http_redirect_url,
				         redirect_sz,
				         "http://%s%s",
				         WIFI_AP_DEFAULT_IP,
				         WEB_APP_LOCATION);
			}

			/* generate the other pages URLs*/
			http_js_url = http_server_generate_url(page_js);
			http_css_url = http_server_generate_url(page_css);
			http_connect_url = http_server_generate_url(page_connect);
			http_ap_url = http_server_generate_url(page_ap);
			http_status_url = http_server_generate_url(page_status);
//			http_mac_url = http_server_generate_url(page_mac);
		}

		err = httpd_port_start(&httpd_handle, &config);

		if (err == APP_OK) {
			APP_LOG(LOG_INFO, FMT_NONE("Registering URI handlers"));
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_get_index);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_get_css);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_get_js);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_get_ap);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_get_status);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_post_connect);
			(void) httpd_port_register_uri_handler(httpd_handle,
			                                       &http_server_delete_connect);
		}
	}
	return;
}

