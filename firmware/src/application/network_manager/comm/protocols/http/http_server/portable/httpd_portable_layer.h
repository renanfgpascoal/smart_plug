/// @file wifi_portable_layer.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "application.h"

#ifdef APP_TRACE_POSIX

#else /* APP_TRACE_POSIX */

#include "esp_wifi.h"
#include "esp_netif.h"
#include "esp_http_server.h"

#endif /* APP_TRACE_POSIX */

/* Module functioning includes end */

/*******************************************************************
 * POST INCLUDES EXTERNED DEFINES
 *******************************************************************/

#ifdef APP_TRACE_POSIX
#define HTTPD_MAX_URI_LEN 512

#define HTTPD_PORT_DEFAULT_CONFIG() {                        \
				.task_priority      = tskIDLE_PRIORITY+5,       \
				.stack_size         = 4096,                     \
				.server_port        = 80,                       \
				.ctrl_port          = 32768,                    \
				.max_open_sockets   = 7,                        \
				.max_uri_handlers   = 8,                        \
				.max_resp_headers   = 8,                        \
				.backlog_conn       = 5,                        \
				.lru_purge_enable   = false,                    \
				.recv_wait_timeout  = 5,                        \
				.send_wait_timeout  = 5,                        \
				.global_user_ctx = NULL,                        \
				.global_user_ctx_free_fn = NULL,                \
				.global_transport_ctx = NULL,                   \
				.global_transport_ctx_free_fn = NULL,           \
				.open_fn = NULL,                                \
				.close_fn = NULL,                               \
}

#define HTTP_METHOD_MAP(XX)         \
  XX(0,  DELETE,      DELETE)       \
  XX(1,  GET,         GET)          \
  XX(2,  HEAD,        HEAD)         \
  XX(3,  POST,        POST)         \
  XX(4,  PUT,         PUT)          \
  /* pathological */                \
  XX(5,  CONNECT,     CONNECT)      \
  XX(6,  OPTIONS,     OPTIONS)      \
  XX(7,  TRACE,       TRACE)        \
  /* WebDAV */                      \
  XX(8,  COPY,        COPY)         \
  XX(9,  LOCK,        LOCK)         \
  XX(10, MKCOL,       MKCOL)        \
  XX(11, MOVE,        MOVE)         \
  XX(12, PROPFIND,    PROPFIND)     \
  XX(13, PROPPATCH,   PROPPATCH)    \
  XX(14, SEARCH,      SEARCH)       \
  XX(15, UNLOCK,      UNLOCK)       \
  XX(16, BIND,        BIND)         \
  XX(17, REBIND,      REBIND)       \
  XX(18, UNBIND,      UNBIND)       \
  XX(19, ACL,         ACL)          \
  /* subversion */                  \
  XX(20, REPORT,      REPORT)       \
  XX(21, MKACTIVITY,  MKACTIVITY)   \
  XX(22, CHECKOUT,    CHECKOUT)     \
  XX(23, MERGE,       MERGE)        \
  /* upnp */                        \
  XX(24, MSEARCH,     M-SEARCH)     \
  XX(25, NOTIFY,      NOTIFY)       \
  XX(26, SUBSCRIBE,   SUBSCRIBE)    \
  XX(27, UNSUBSCRIBE, UNSUBSCRIBE)  \
  /* RFC-5789 */                    \
  XX(28, PATCH,       PATCH)        \
  XX(29, PURGE,       PURGE)        \
  /* CalDAV */                      \
  XX(30, MKCALENDAR,  MKCALENDAR)   \
  /* RFC-2068, section 19.6.1.2 */  \
  XX(31, LINK,        LINK)         \
  XX(32, UNLINK,      UNLINK)       \

#else /* APP_TRACE_POSIX */

#define HTTPD_PORT_DEFAULT_CONFIG() 	HTTPD_DEFAULT_CONFIG()

#endif /* APP_TRACE_POSIX */

#define HTTP_METHOD_HANDLER(HANDLER)	app_err_t (HANDLER)(httpd_port_req_t *p_from)

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

typedef void* httpd_port_handle_t;

#else /* APP_TRACE_POSIX */

typedef httpd_handle_t httpd_port_handle_t;

#endif /* APP_TRACE_POSIX */
#ifdef APP_TRACE_POSIX

typedef void (*httpd_free_ctx_fn_t)(void *ctx);

typedef struct httpd_req {
		httpd_port_handle_t  handle;                     /*!< Handle to server instance */
    int             method;                     /*!< The type of HTTP request, -1 if unsupported method */
    const char      uri[HTTPD_MAX_URI_LEN + 1]; /*!< The URI of this request (1 byte extra for null termination) */
    size_t          content_len;                /*!< Length of the request body */
    void            *aux;                        /*!< Internally used members */
    void 					  *user_ctx;
    void 						*sess_ctx;
    httpd_free_ctx_fn_t free_ctx;
} httpd_req_dummy_t;

typedef httpd_req_dummy_t httpd_port_req_t;

#else /* APP_TRACE_POSIX */

typedef httpd_req_t httpd_port_req_t;

#endif /* APP_TRACE_POSIX */

#ifdef APP_TRACE_POSIX

typedef app_err_t (*httpd_open_func_t)(httpd_port_handle_t hd, int sockfd);
typedef void (*httpd_close_func_t)(httpd_port_handle_t hd, int sockfd);

typedef struct httpd_config {
		unsigned    task_priority;      /*!< Priority of FreeRTOS task which runs the server */
		size_t      stack_size;         /*!< The maximum stack size allowed for the server task */
		uint16_t    server_port;
		uint16_t    ctrl_port;
		uint16_t    max_open_sockets;   /*!< Max number of sockets/clients connected at any time*/
		uint16_t    max_uri_handlers;   /*!< Maximum allowed uri handlers */
		uint16_t    max_resp_headers;   /*!< Maximum allowed additional headers in HTTP response */
		uint16_t    backlog_conn;       /*!< Number of backlog connections */
		bool        lru_purge_enable;   /*!< Purge "Least Recently Used" connection */
		uint16_t    recv_wait_timeout;  /*!< Timeout for recv function (in seconds)*/
		uint16_t    send_wait_timeout;  /*!< Timeout for send function (in seconds)*/
		void * global_user_ctx;
		httpd_free_ctx_fn_t global_user_ctx_free_fn;
		void * global_transport_ctx;
		httpd_free_ctx_fn_t global_transport_ctx_free_fn;
		httpd_open_func_t open_fn;
		httpd_close_func_t close_fn;
} httpd_config_dummy_t;

typedef httpd_config_dummy_t httpd_port_config_t;

#else /* APP_TRACE_POSIX */

typedef httpd_config_t httpd_port_config_t;

#endif /* APP_TRACE_POSIX */

#ifdef APP_TRACE_POSIX

enum http_method
  {
#define XX(num, name, string) HTTP_##name = num,
  HTTP_METHOD_MAP(XX)
#undef XX
  };
typedef enum http_method httpd_method_t;

typedef struct httpd_uri {
		const char       *uri;    /*!< The URI to handle */
		httpd_method_t    method; /*!< Method supported by the URI */
		app_err_t (*handler)(httpd_port_req_t *r);
		void *user_ctx;
} httpd_uri_dummy_t;

typedef httpd_uri_dummy_t httpd_port_uri_t;

#else /* APP_TRACE_POSIX */

typedef httpd_uri_t httpd_port_uri_t;

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX


#else

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Checks if wifi is connected
 * @param timeout_ms Timeout value in milliseconds
 * @return True if connected, false otherwise
 */
app_err_t httpd_port_resp_set_status(httpd_port_req_t *p_req,
                                     const char *p_status);

app_err_t httpd_port_resp_set_type(httpd_port_req_t *p_req, const char *p_type);

app_err_t httpd_port_resp_set_hdr(httpd_port_req_t *p_req,
                                  const char *p_field,
                                  const char *p_value);

app_err_t httpd_port_resp_send(httpd_port_req_t *p_req,
                               const char *p_buf,
                               uint32_t buf_len);

uint32_t httpd_port_req_get_hdr_len(httpd_port_req_t *p_req,
                                    const char *p_field);

app_err_t httpd_port_req_get_hdr_value_str(httpd_port_req_t *p_req,
                                           const char *p_field,
                                           char *p_val,
                                           size_t val_size);

app_err_t httpd_port_req_to_sockfd(httpd_port_req_t *p_req);

int32_t httpd_port_req_recv(httpd_port_req_t *p_req,
                            char *p_buf,
                            uint32_t buf_len);

app_err_t httpd_port_sess_close(httpd_port_handle_t handle, int sockfd);

app_err_t httpd_port_register_uri_handler(httpd_port_handle_t handle,
                                          const httpd_port_uri_t *p_uri_handler);

app_err_t httpd_port_stop(httpd_port_handle_t handle);

app_err_t httpd_port_start(httpd_port_handle_t *p_handle,
                           const httpd_port_config_t *p_config);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
