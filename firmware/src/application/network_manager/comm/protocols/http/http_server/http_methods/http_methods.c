///@file network_mgr_application.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "../http_methods/http_methods.h"
#include "wifi_application_layer.h"
#include "proprietary_utils.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum http_resp_type_t {
	HTTP_RESP_NO_TYPE,
	HTTP_RESP_JSON,
	HTTP_RESP_HTML,
	HTTP_RESP_JS,
	HTTP_RESP_CSS,
	HTTP_RESP_REDIRECT,
	HTTP_RESP_BAD_REQUEST,
	HTTP_RESP_NOT_FOUND,
	HTTP_RESP_NO_SERVICE,
	MAX_HTTP_RESP_TYPES,
} http_resp_type_t;

typedef struct http_resp_content_t {
	const char *p_status;
	http_resp_type_t type;
	const char *p_content;
	uint32_t content_size;
} http_resp_content_t;

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

extern const uint8_t style_css_start[] asm("_binary_style_css_start");
extern const uint8_t style_css_end[] asm("_binary_style_css_end");
extern const uint8_t code_js_start[] asm("_binary_code_js_start");
extern const uint8_t code_js_end[] asm("_binary_code_js_end");
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");

extern char *http_root_url;
extern char *http_redirect_url;
extern char *http_js_url;
extern char *http_css_url;
extern char *http_connect_url;
extern char *http_ap_url;
extern char *http_status_url;
//extern char *http_mac_url;

/* const httpd related values stored in ROM */
const static char http_200_hdr[] = "200 OK";
//const static char http_302_hdr[] = "302 Found";
const static char http_400_hdr[] = "400 Bad Request";
const static char http_404_hdr[] = "404 Not Found";
const static char http_503_hdr[] = "503 Service Unavailable";
const static char http_location_hdr[] = "location";
const static char http_content_type_html[] = "text/html";
const static char http_content_type_js[] = "text/javascript";
const static char http_content_type_css[] = "text/css";
const static char http_content_type_json[] = "application/json";
const static char http_cache_control_hdr[] = "Cache-Control";
const static char http_cache_control_no_cache[] = "no-store, no-cache, must-revalidate, max-age=0";
const static char http_cache_control_cache[] = "public, max-age=31536000";
const static char http_pragma_hdr[] = "Pragma";
const static char http_pragma_no_cache[] = "no-cache";

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static bool http_methods_send_response(httpd_port_req_t *p_from,
                                       http_resp_content_t *p_content) {
	DEVELOPMENT_ASSERT(p_from != NULL, 0xFFFFFFFF);
	DEVELOPMENT_ASSERT(p_content != NULL, 0xFFFFFFFF);

	if (httpd_port_resp_set_status(p_from, p_content->p_status) != APP_OK) {
		return false;
	}

	switch(p_content->type) {
		case HTTP_RESP_JSON: {
			if (httpd_port_resp_set_type(p_from, http_content_type_json) != APP_OK) {
				return false;
			}

			if (httpd_port_resp_set_hdr(p_from,
			                       http_cache_control_hdr,
			                       http_cache_control_no_cache) != APP_OK) {
				return false;
			}

			if (httpd_port_resp_set_hdr(p_from, http_pragma_hdr, http_pragma_no_cache) != APP_OK) {
				return false;
			}

			break;
		}
		case HTTP_RESP_HTML: {
			if (httpd_port_resp_set_type(p_from, http_content_type_html) != APP_OK) {
				return false;
			}

			break;
		}
		case HTTP_RESP_JS: {
			if (httpd_port_resp_set_type(p_from, http_content_type_js) != APP_OK) {
				return false;
			}

			break;
		}
		case HTTP_RESP_CSS: {
			if (httpd_port_resp_set_type(p_from, http_content_type_css) != APP_OK) {
				return false;
			}

			if (httpd_port_resp_set_hdr(p_from,
			                       http_cache_control_hdr,
			                       http_cache_control_cache) != APP_OK) {
				return false;
			}

			break;
		}
		case HTTP_RESP_REDIRECT: {
			httpd_port_resp_set_hdr(p_from, http_location_hdr, http_redirect_url);

			break;
		}
		case HTTP_RESP_BAD_REQUEST:
		case HTTP_RESP_NOT_FOUND:
		case HTTP_RESP_NO_SERVICE: {

			break;
		}
		default: {
			APP_LOG(LOG_WARN, FMT_ARGS("Unhandled response type: [%u]", p_content->type));

			return false;
		}
	}

	if (httpd_port_resp_send(p_from, p_content->p_content, p_content->content_size) != APP_OK) {
		return false;
	}

	return true;
}

HTTP_METHOD_HANDLER(http_method_get_index) {
	DEVELOPMENT_ASSERT(p_from != NULL, APP_FAIL);

	APP_LOG(LOG_WARN, FMT_ARGS("GET %s", p_from->uri));

	/* Get header value string length and allocate memory for length + 1,
	 * extra byte for null termination */
	char *host = NULL;
	uint32_t buf_len = httpd_port_req_get_hdr_len(p_from, "Host") + 1;

	if (buf_len <= 1) {
		return APP_FAIL;
	}

	host = malloc(buf_len);
	if (httpd_port_req_get_hdr_value_str(p_from, "Host", host, buf_len) != APP_OK) {
		/* if something is wrong we just 0 the whole memory */
		memset(host, 0x00, buf_len);
	}

	/* determine if Host is from the AP IP address */
	bool access_from_ap_ip = strstr(host, WIFI_AP_DEFAULT_IP) != NULL;
	free(host);

	APP_LOG(LOG_WARN, FMT_ARGS("[%u]", access_from_ap_ip));

	http_resp_content_t content;

#if 1

	if (strcmp(p_from->uri, http_root_url) == 0) {
		content.p_status = http_200_hdr;
		content.type = HTTP_RESP_HTML;
		content.p_content = (char*) index_html_start;
		content.content_size = index_html_end - index_html_start;
		if (http_methods_send_response(p_from, &content) == false) {
			return APP_FAIL;
		}

#else
		if (access_from_ap_ip == true) {
			content.p_status = http_302_hdr;
			content.type = HTTP_RESP_REDIRECT;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				return APP_FAIL;
			}
		} else {
			if (strcmp(p_from->uri, http_root_url) != 0) {
				content.p_status = http_400_hdr;
				content.type = HTTP_RESP_BAD_REQUEST;
				content.p_content = NULL;
				content.content_size = 0;
				if (http_methods_send_response(p_from, &content) == false) {
					return APP_FAIL;
				}
			}

			content.p_status = http_200_hdr;
			content.type = HTTP_RESP_HTML;
			content.p_content = (char*) index_html_start;
			content.content_size = index_html_end - index_html_start;
			if (http_methods_send_response(p_from, &content) == false) {
				return APP_FAIL;
			}
		}

#endif
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			return APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return APP_OK;
}

HTTP_METHOD_HANDLER(http_method_get_js) {
	DEVELOPMENT_ASSERT(p_from != NULL, APP_FAIL);

	APP_LOG(LOG_WARN, FMT_ARGS("GET %s", p_from->uri));

	http_resp_content_t content;
	if (strcmp(p_from->uri, http_js_url) == 0) {
		content.p_status = http_200_hdr;
		content.type = HTTP_RESP_JS;
		content.p_content = (char*) code_js_start;
		content.content_size = code_js_end - code_js_start;
		if (http_methods_send_response(p_from, &content) == false) {
			return APP_FAIL;
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			return APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return APP_OK;
}

HTTP_METHOD_HANDLER(http_method_get_css) {
	DEVELOPMENT_ASSERT(p_from != NULL, APP_FAIL);

	APP_LOG(LOG_WARN, FMT_ARGS("GET %s", p_from->uri));

	app_err_t err = APP_OK;
	http_resp_content_t content;

	if (strcmp(p_from->uri, http_css_url) == 0) {
		content.p_status = http_200_hdr;
		content.type = HTTP_RESP_CSS;
		content.p_content = (char*) style_css_start;
		content.content_size = style_css_end - style_css_start;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return err;
}

HTTP_METHOD_HANDLER(http_method_get_ap) {
	DEVELOPMENT_ASSERT(p_from != NULL, APP_FAIL);

	APP_LOG(LOG_WARN, FMT_ARGS("GET %s", p_from->uri));

	app_err_t err = APP_OK;
	http_resp_content_t content;

	if (strcmp(p_from->uri, http_ap_url) == 0) {
		char *ap_buf = wifi_app_layer_get_scan_list(MS(100));
		if (ap_buf != NULL) {
			content.p_status = http_200_hdr;
			content.type = HTTP_RESP_JSON;
			content.p_content = ap_buf;
			content.content_size = strlen(ap_buf);
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}
		} else {
			content.p_status = http_503_hdr;
			content.type = HTTP_RESP_NO_SERVICE;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}
			APP_LOG(LOG_ERROR, FMT_NONE("http_server_netconn_serve: GET /ap.json failed to obtain mutex"));
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	}

	/* request a wifi scan */
	wifi_app_layer_start_scan_ap(MS(100));
	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return err;
}

HTTP_METHOD_HANDLER(http_method_get_status) {
	DEVELOPMENT_ASSERT(p_from != NULL, APP_FAIL);

	APP_LOG(LOG_WARN, FMT_ARGS("GET %s", p_from->uri));

	app_err_t err = APP_OK;
	http_resp_content_t content;

	if (strcmp(p_from->uri, http_status_url) == 0) {
		char *buff = wifi_app_layer_get_sta_ip_info(MS(100));
		if (buff != NULL) {
			if (buff) {
				content.p_status = http_200_hdr;
				content.type = HTTP_RESP_JSON;
				content.p_content = buff;
				content.content_size = strlen(buff);
				if (http_methods_send_response(p_from, &content) == false) {
					err = APP_FAIL;
				}
			} else {
				content.p_status = http_503_hdr;
				content.type = HTTP_RESP_NO_SERVICE;
				content.p_content = NULL;
				content.content_size = 0;
				if (http_methods_send_response(p_from, &content) == false) {
					err = APP_FAIL;
				}
			}
		} else {
			content.p_status = http_503_hdr;
			content.type = HTTP_RESP_NO_SERVICE;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}

			APP_LOG(LOG_ERROR, FMT_NONE("http_server_netconn_serve: GET /status.json failed to obtain mutex"));
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return err;
}

HTTP_METHOD_HANDLER(http_method_post_connect) {
	APP_LOG(LOG_WARN, FMT_ARGS("POST %s", p_from->uri));

	app_err_t err = APP_OK;
	http_resp_content_t content;

	if (strcmp(p_from->uri, http_connect_url) == 0) {
		if (p_from->content_len > 0) {
			if (wifi_app_connect() == true) {
				content.p_status = http_200_hdr;
				content.type = HTTP_RESP_JSON;
				content.p_content = NULL;
				content.content_size = 0;
				if (http_methods_send_response(p_from, &content) == false) {
					err = APP_FAIL;
				}
			} else{
				content.p_status = http_503_hdr;
				content.type = HTTP_RESP_NO_SERVICE;
				content.p_content = NULL;
				content.content_size = 0;
				if (http_methods_send_response(p_from, &content) == false) {
					err = APP_FAIL;
				}
			}
		} else {
			/* bad request the authentification header is not complete/not the correct format */
			content.p_status = http_400_hdr;
			content.type = HTTP_RESP_BAD_REQUEST;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return err;
}

HTTP_METHOD_HANDLER(http_method_delete_connect) {
	APP_LOG(LOG_WARN, FMT_ARGS("DELETE %s", p_from->uri));

	app_err_t err = APP_OK;
	http_resp_content_t content;

	if (strcmp(p_from->uri, http_connect_url) == 0) {
		if (wifi_app_disconnect() == true) {
			content.p_status = http_200_hdr;
			content.type = HTTP_RESP_JSON;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}
		} else{
			content.p_status = http_503_hdr;
			content.type = HTTP_RESP_NO_SERVICE;
			content.p_content = NULL;
			content.content_size = 0;
			if (http_methods_send_response(p_from, &content) == false) {
				err = APP_FAIL;
			}
		}
	} else {
		content.p_status = http_404_hdr;
		content.type = HTTP_RESP_NOT_FOUND;
		content.p_content = NULL;
		content.content_size = 0;
		if (http_methods_send_response(p_from, &content) == false) {
			err = APP_FAIL;
		}
	}

	int sockfd = httpd_port_req_to_sockfd(p_from);
	(void) httpd_port_sess_close(p_from->handle, sockfd);

	return err;
}
