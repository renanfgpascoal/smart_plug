///@file httpd_esp8266.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include <esp_http_server.h>

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "httpd_portable_layer.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

app_err_t httpd_port_resp_set_status(httpd_port_req_t *p_req, const char *p_status) {
	if (httpd_resp_set_status(p_req, p_status) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_resp_set_type(httpd_port_req_t *p_req, const char *p_type) {
	if (httpd_resp_set_type(p_req, p_type) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_resp_set_hdr(httpd_port_req_t *p_req,
                                  const char *p_field,
                                  const char *p_value) {
	if (httpd_resp_set_hdr(p_req, p_field, p_value) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_resp_send(httpd_port_req_t *p_req,
                               const char *p_buf,
                               uint32_t buf_len) {
	if (httpd_resp_send(p_req, p_buf, buf_len) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

uint32_t httpd_port_req_get_hdr_len(httpd_port_req_t *p_req, const char *p_field) {
	return (uint32_t) httpd_req_get_hdr_value_len(p_req, p_field);
}

app_err_t httpd_port_req_get_hdr_value_str(httpd_port_req_t *p_req,
                                           const char *p_field,
                                           char *p_val,
                                           size_t val_size) {
	if (httpd_req_get_hdr_value_str(p_req, p_field, p_val, val_size) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_req_to_sockfd(httpd_port_req_t *p_req) {
	if (httpd_req_to_sockfd(p_req) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

int32_t httpd_port_req_recv(httpd_port_req_t *p_req, char *p_buf, uint32_t buf_len) {
	return httpd_req_recv(p_req, p_buf, (size_t) buf_len);
}

app_err_t httpd_port_sess_close(httpd_port_handle_t handle, int sockfd) {
	if (httpd_sess_trigger_close(handle, sockfd) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_register_uri_handler(httpd_port_handle_t handle,
                                          const httpd_port_uri_t *p_uri_handler) {
	if (httpd_register_uri_handler(handle, p_uri_handler) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_stop(httpd_port_handle_t handle) {
	if (httpd_stop(handle) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t httpd_port_start(httpd_port_handle_t *p_handle,
                           const httpd_port_config_t *p_config) {
	if (httpd_start(p_handle, p_config) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}
