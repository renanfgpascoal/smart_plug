/// @file application.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <stdbool.h>

#ifndef UNIT_TESTS
#ifdef APP_TRACE_POSIX

#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"
#include "semphr.h"
#include "posix_support.h"

#else

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#endif /* APP_TRACE_POSIX */
#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "proprietary_assert.h"
#include "app_err.h"

#ifndef UNIT_TESTS
#include "fw_config.h"

#ifdef APP_TRACE_POSIX

#include "app_trace.h"

#endif /* APP_TRACE_POSIX */
#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */
#ifndef UNIT_TESTS
#if DEBUG_PRINTS_ENABLE == 1
#ifdef APP_TRACE_POSIX

/*!
 * @brief Defines Application log print functionality
 * @param FUNC Function to use as printing function
 * @param ... Variable arguments to pass to printing function
 */
#define APP_LOG(FUNC, ...)	\
		FUNC __VA_ARGS__)

#else

/*!
 * @brief Defines Application log print functionality
 * @param FUNC Function to use as printing function
 * @param ... Variable arguments to pass to printing function
 */
#define APP_LOG(FUNC, ...)	\
		FUNC(__VA_ARGS__)

#endif /* APP_TRACE_POSIX */

#else /* DEBUG_PRINTS_ENABLE == 1 */

/*! Disabled Console trace functionality */
#define APP_LOG(FUNC, ...)

#endif /* DEBUG_PRINTS_ENABLE == 1 */

#ifdef APP_TRACE_POSIX

#if APP_LOG_SHOW_TIME == 1
/*!
 * @brief Edits prints (WITH VARIABLE ARGUMENTS, MANDATORY) to an expected pattern
 * @param STRING String to be displayed
 * @param ... Variable arguments list
 */
#define FMT_ARGS(STRING, ...) \
		 "%ld [ms] %s: (%u) - " STRING "\n", GET_TICK(), (const char *)&__FUNCTION__, __LINE__, __VA_ARGS__

/*!
 * @brief Edits prints without any arguments
 * @param STRING String to format
 */
#define FMT_NONE(STRING)				\
		"%ld [ms] %s: (%u) - " STRING "\n", GET_TICK(), (const char *)&__FUNCTION__, __LINE__

#else

/*!
 * @brief Edits prints (WITH VARIABLE ARGUMENTS, MANDATORY) to an expected pattern
 * @param STRING String to be displayed
 * @param ... Variable arguments list
 */
#define FMT_ARGS(STRING, ...) \
		 "%s: (%u) - " STRING "\n", (const char *)&__FUNCTION__, __LINE__, __VA_ARGS__

/*!
 * @brief Edits prints without any arguments
 * @param STRING String to format
 */
#define FMT_NONE(STRING)				\
		"%s: (%u) - " STRING "\n", (const char *)&__FUNCTION__, __LINE__

#endif /* APP_LOG_SHOW_TIME == 1 */

/*! Defines print function for information log  */
#define LOG_INFO										printf("\033[0;32m"

/*! Defines print function for warning log  */
#define LOG_WARN										printf("\033[0;33m"

/*! Defines print function for error log  */
#define LOG_ERROR										printf("\033[0;31m"

/*! Defines print function for trace log  */
#define LOG_TRACE										TRACE_PRINT

#else

/*!
 * @brief Edits prints (WITH VARIABLE ARGUMENTS, MANDATORY) to an expected pattern
 * @param STRING String to be displayed
 * @param ... Variable arguments list
 */
#define FMT_ARGS(STRING, ...) \
		(const char *)&__FUNCTION__, "(%u) - " STRING, __LINE__, __VA_ARGS__

/*!
 * @brief Edits prints without any arguments
 * @param STRING String to format
 */
#define FMT_NONE(STRING)				\
		(const char *)&__FUNCTION__, "(%u) - " STRING, __LINE__

/*! Defines print function for information log  */
#define LOG_INFO										ESP_LOGI

/*! Defines print function for warning log  */
#define LOG_WARN										ESP_LOGW

/*! Defines print function for error log  */
#define LOG_ERROR										ESP_LOGE

/*! Defines print function for trace log  */
#define LOG_TRACE										ESP_LOGW

#endif /* APP_TRACE_POSIX */
/*!
 * @brief Type of event flags handle
 */
#define EVENT_FLAG_HANDLE_TYPE	\
		EventGroupHandle_t

#define EVENT_FLAGS_TYPE	\
		EventBits_t

#define TASK_HANDLE_TYPE	\
		TaskHandle_t

#define SEMAPHORE_HANDLE_TYPE	\
		SemaphoreHandle_t

#define CREATE_EVENT_FLAG()	\
		xEventGroupCreate()

#define DELETE_EVENT_FLAG(HANDLE)	\
		vEventGroupDelete((EVENT_FLAG_HANDLE_TYPE)(HANDLE))

#define SET_EVENT_FLAG(HANDLE, FLAGS_TO_SET)	\
		(void) xEventGroupSetBits((EventGroupHandle_t)(HANDLE), \
		                          (const EventBits_t)(FLAGS_TO_SET))

#define CLR_EVENT_FLAG(HANDLE, FLAGS_TO_CLEAR)	\
		(void) xEventGroupClearBits((EventGroupHandle_t)(HANDLE), \
		                            (const EventBits_t)(FLAGS_TO_CLEAR))

#define WAIT_EVENT_FLAG(HANDLE, FLAGS_TO_WAIT, CLR_ON_EXIT, WAIT_ALL_FLAGS, TIMEOUT)	\
		xEventGroupWaitBits((EventGroupHandle_t)(HANDLE), \
		                    (const EventBits_t)(FLAGS_TO_WAIT), \
		                    (const BaseType_t)(CLR_ON_EXIT), \
		                    (const BaseType_t)(WAIT_ALL_FLAGS), \
		                    (TickType_t)(TIMEOUT))

#define CHECK_ONE_EVENT_FLAG(HANDLE, FLAGS_TO_CHECK, TIMEOUT)	\
		WAIT_EVENT_FLAG((HANDLE), (FLAGS_TO_CHECK), pdFALSE, pdTRUE, (TIMEOUT))

/*!
 * @brief Get Ticks count since system boot
 */
#define GET_TICK()	\
		xTaskGetTickCount()

/*!
 * @brief Relative delay
 * @param TIMEOUT_MS Timeout time, in milliseconds
 */
#define RELATIVE_DELAY(TIMEOUT_MS) \
		vTaskDelay(pdMS_TO_TICKS(TIMEOUT_MS))

/*!
 * @brief Absolute delay
 * @param P_START Variable holding absolute time of delay start
 * @param TIMEOUT_MS Timeout time, in milliseconds
 */
#define ABSOLUTE_DELAY(P_START, TIMEOUT_MS) \
		vTaskDelayUntil(P_START, pdMS_TO_TICKS(TIMEOUT_MS))

/*!
 * @brief Creates a static allocated task
 * @param FUNCTION Task function name (not string!)
 * @param STACK Task stack name (not string!)
 * @param P_PARAM Address of a parameter variable to be passed as task parameter
 * @param PRIO Task priority value
 * @param P_TCB Address of a variable to hold the task control block
 */
#define TASK_STATIC(FUNCTION, STACK, SIZE, P_PARAM, PRIO, P_TCB) \
		xTaskCreateStatic(FUNCTION, \
		                  #FUNCTION, \
		                  SIZE, \
		                  P_PARAM, \
		                  PRIO, \
		                  &STACK[0], \
		                  P_TCB)

/*!
 * @brief Creates a dynamic allocated task
 * @param FUNCTION Task function
 * @param SIZE Task stack size
 * @param P_PARAM Address of a parameter variable to be passed as task parameter
 * @param PRIO Task priority value
 * @param P_TASK_HANDLE Address of task handle
 */
#define TASK_DYNAMIC(FUNCTION, SIZE, P_PARAM, PRIO, P_TASK_HANDLE) \
		xTaskCreate(FUNCTION, \
								#FUNCTION, \
								SIZE, \
								P_PARAM, \
								PRIO, \
								P_TASK_HANDLE)
/*!
 * @brief Deletes a task
 * @param HANDLE Task handle for deletion
 */
#define TASK_DELETE(HANDLE) \
		vTaskDelete(HANDLE)

#define TASK_NOTIFY(TO, VALUE) \
	xTaskNotify((TO), (VALUE), eSetValueWithoutOverwrite)

#define TASK_WAIT_NOTIFY(P_TO, TIMEOUT_MS) \
	xTaskNotifyWait(0, 0, (P_TO), (TIMEOUT_MS))

/*!
 * @brief Notify an event from an ISR
 * @param P_TO Handle of the task to notify, in case of using an RTOS
 * @param VALUE Value of the event to notify
 * @param TYPE Type of action to perform on the value of the receiving end
 */
#define TASK_NOTIFY_ISR(TO, VALUE) \
		do { \
			BaseType_t unused = pdFALSE; \
			(void) xTaskNotifyFromISR(TO, VALUE, eSetValueWithoutOverwrite, &unused); \
		} while(0)

/*!
 * @brief Yields a OS Task from an ISR
 */
#define YIELD_ISR() \
		portYIELD_FROM_ISR()

/*!
 * @brief Creates a static queue
 * @param BUFFER Buffer to hold queue items
 * @param P_QUEUE Address of structure to hold the queue
 */
#define QUEUE_STATIC(BUFFER, P_QUEUE) \
		xQueueCreateStatic(ARRAY_SIZE(BUFFER), \
		                   sizeof((BUFFER)[0]), \
		                   (uint8_t *)&(BUFFER)[0], \
		                   P_QUEUE)

/*!
 * @brief Pushes an data to a queue
 * @param P_FROM Address of where to push data from
 * @param P_TO Address of queue to push data to
 * @param TIMEOUT_MS Timeout to push data to queue, in milliseconds
 */
#define QUEUE_PUSH(P_FROM, P_TO, TIMEOUT_MS) \
		xQueueSend(P_TO, (void *)P_FROM, pdMS_TO_TICKS(TIMEOUT_MS))

/*!
 * @brief Pulls data from a queue
 * @param P_FROM Address of where to pull data from
 * @param P_TO Address of structure to pull data to
 * @param TIMEOUT_MS Timeout to push data to queue, in milliseconds
 */
#define QUEUE_PULL(P_FROM, P_TO, TIMEOUT_MS) \
		xQueueReceive(P_FROM, (void *)P_TO, pdMS_TO_TICKS(TIMEOUT_MS))

/*!
 * @brief Flushed a queue
 * @param P_TO Address of queue to flush
 */
#define QUEUE_FLUSH(P_TO) 						xQueueReset(P_TO)

/*!
 * @brief Creates a static semaphore
 * @param P_BUFFER Address of buffer to hold sempahore item
 */
#define SEMAPHORE_DYNAMIC() 		xSemaphoreCreateBinary()

/*!
 * @brief Takes a semaphore
 * @param P_FROM Address of structure holding parameters of semaphore to be taken from
 * @param TIMEOUT_MS Timeout to try take semaphore , in milliseconds
 */
#define SEMAPHORE_TAKE(P_FROM, TIMEOUT_MS) \
		xSemaphoreTake((QueueHandle_t)P_FROM, pdMS_TO_TICKS(TIMEOUT_MS))

/*!
 * @brief Takes a semaphore from ISR
 * @param P_FROM Address of structure holding parameters of semaphore to be taken from
 */
#define SEMAPHORE_TAKE_ISR(P_FROM) \
		xSemaphoreTakeFromISR((QueueHandle_t)P_FROM, NULL)

/*!
 * @brief Give a taken semaphore
 * @param P_TO Address of structure holding parameters of semaphore to give back to
 */
#define SEMAPHORE_GIVE(P_TO) 					xSemaphoreGive((QueueHandle_t)P_TO)

/*!
 * @brief Give a taken semaphore from ISR
 * @param P_TO Address of structure holding parameters of semaphore to give back to
 */
#define SEMAPHORE_GIVE_ISR(P_TO) \
	xSemaphoreGiveFromISR((QueueHandle_t)P_TO, NULL)


/*!
 * @brief Creates a static instance of a Timer
 * @param P_NAME Name of the timer instance
 * @param TIMEOUT_MS Timer timeout, in milliseconds
 * @param AUTORELOAD Flag signaling if timer is of outo reload type (pdTRUE)
 * @param P_CALLBACK Address of callback function
 * @param P_BUFFER Address of timer instance buffer to hold instantiated buffer
 */
#define TIMER_OS_CREATE(P_NAME, TIMEOUT_MS, AUTORELOAD, P_CALLBACK) \
		xTimerCreate(P_NAME, \
		                   pdMS_TO_TICKS(TIMEOUT_MS), \
		                   AUTORELOAD, \
		                   NULL, \
		                   P_CALLBACK)

/*!
 * @brief Starts an instance of a Timer (OS is to differentiate from possible HAL's TIMER_START
 * @param P_FROM Handle of Timer's instance to start
 * @param DELAY_MS Timer delay to start Timer instance, in milliseconds
 */
#define TIMER_OS_START(P_FROM, DELAY_MS) \
		xTimerStart(P_FROM, pdMS_TO_TICKS(DELAY_MS))

/*!
 * @brief Starts an instance of a Timer from a ISR
 * @param P_FROM Handle of Timer's instance to start
 * @param DELAY_MS Timer delay to start the Timer instance, in milliseconds
 */
#define TIMER_OS_START_ISR(P_FROM, DELAY_MS) \
		xTimerStartFromISR(P_FROM, pdMS_TO_TICKS(DELAY_MS))

/*!
 * @brief Resets an instance of a Timer from a ISR
 * @param P_FROM Handle of Timer's instance to start
 */
#define TIMER_OS_RESET_ISR(P_FROM) \
		xTimerResetFromISR(P_FROM, NULL)

/*!
 * @brief Stops an instance of a Timer
 * @param P_FROM Handle of Timer's instance to start
 * @param DELAY_MS Timer delay to stop the Timer instance, in milliseconds
 */
#define TIMER_OS_STOP(P_FROM, DELAY_MS) \
		xTimerStop(P_FROM, pdMS_TO_TICKS(DELAY_MS))

/*!
 * @brief Stops an instance of a Timer from ISR
 * @param P_FROM Handle of Timer's instance to start
 * @param DELAY_MS Timer delay to stop the Timer instance, in milliseconds
 */
#define TIMER_OS_STOP_ISR(P_FROM) \
		xTimerStopFromISR(P_FROM, NULL)

#ifdef APP_TRACE_POSIX

#define SYSTEM_RESTART()	\
	do{	\
		exit(0);	\
	}	\
	while(0)

#else

#define SYSTEM_RESTART()	\
		esp_restart()

#endif /* APP_TRACE_POSIX */

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Structure holding application information  */
typedef struct app_info_t {
	/*! Specify if this task is critical for main application running */
	bool is_critical;
	/*! Killed times of this task */
	uint32_t killed_times;
}app_info_t;

/*! Structure holding Address of Queues Handlers generated for applications data transfer */
typedef struct app_req_res_queues_t {
	/*! Request Queue */
	QueueHandle_t p_req_handle;
	/*! Response Queue */
	QueueHandle_t p_res_handle;
}app_req_res_queues_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

/*! Application basic definitions (mostly task related) */
typedef enum app_defs_t {
	/*! Applications base priority */
	APP_BASE_PRIORITY = 1,
	APP_MAX_PRIORITY = configMAX_PRIORITIES,
	/*! Software watchdog Application task priority */
	SOFT_WDT_APP_PRIORITY = APP_BASE_PRIORITY + 2,
	/*! Network manager Application task priority */
	NETWORK_MGR_APP_PRIORITY = APP_BASE_PRIORITY + 1,
	/*! Fan control Application task priority */
	FAN_CONTROL_APP_PRIORITY = APP_BASE_PRIORITY + 1,
	/*! Wifi events loop task priority */
	WIFI_EVENT_LOOP_PRIORITY = APP_MAX_PRIORITY - 5,
	/*****************************************************************/
	/*! Applications base stack size */
	APP_BASE_STACK_SIZE = 1024,
	APP_BASE_STACK_EXTRA = 512,
	/*! Software watchdog Application stack size */
	SOFT_WDT_APP_STACK_SIZE = APP_BASE_STACK_SIZE,
	/*! Network manager Application stack size */
	NETWORK_MGR_APP_STACK_SIZE = APP_BASE_STACK_SIZE + 1024,
	/*! Fan control Application stack size */
	FAN_CONTROL_APP_STACK_SIZE = APP_BASE_STACK_SIZE,
	/*! Wifi events loop stack size */
	WIFI_EVENT_LOOP_STACK_SIZE = 2 * APP_BASE_STACK_SIZE + APP_BASE_STACK_EXTRA,
	/*****************************************************************/
	/*! Applications base queue size */
	APPS_BASE_QUEUE_SIZE = 1,
	/*! Wifi events loop queue size */
	WIFI_EVENT_LOOP_QUEUE_SIZE = 32 * APPS_BASE_QUEUE_SIZE,
}app_defs_t;

/*! Applications indexes
 * (software watchdog application not included) */
typedef enum app_index_t {
	/*! Network manager application index */
	NETWORK_MGR_APP_IDX,
	/*! Fan control application index */
	FAN_CONTROL_APP_IDX,
	/*! Maximum applications indexes */
	APP_MAX_INDEXES,
}app_index_t;

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
