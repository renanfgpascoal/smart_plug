///@file event_loop_esp8266.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "event_loop_portable_layer.h"

#ifdef APP_TRACE_POSIX

#else

#endif /* APP_TRACE_POSIX */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

app_err_t event_loop_port_event_post(app_event_base_t event_base,
                                     int32_t event_id,
                                     void *p_event_data,
                                     size_t event_data_size,
                                     uint32_t timeout_ms) {
	if (esp_event_post(event_base,
	                   event_id,
	                   p_event_data,
	                   event_data_size,
	                   timeout_ms) != ESP_OK) {
		return APP_FAIL;
	}
	return APP_OK;
}

app_err_t event_loop_port_create_default() {
	if (esp_event_loop_create_default() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t event_loop_port_delete_default() {
	if (esp_event_loop_delete_default() != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t event_loop_port_handler_register(app_event_base_t event_base,
                                           int32_t event_id,
                                           void *event_handler,
                                           void *event_handler_arg) {
	if (esp_event_handler_register(event_base,
	                               event_id,
	                               event_handler,
	                               event_handler_arg) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t event_loop_port_handler_unregister(app_event_base_t event_base,
                                             int32_t event_id,
                                             void *event_handler) {
	if (esp_event_handler_unregister(event_base, event_id, event_handler) != ESP_OK) {
		return APP_FAIL;
	}

	return APP_OK;
}

