///@file event_loop_posix.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/*
 * Singly-linked List definitions.
 */
#define	SLIST_HEAD(name, type)						\
struct name {								\
	struct type *slh_first;	/* first element */			\
}

#define	SLIST_ENTRY(type)						\
struct {								\
	struct type *sle_next;	/* next element */			\
}

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "event_loop_portable_layer.h"
#include "wifi_portable_layer.h"
#include "proprietary_utils.h"

#ifdef APP_TRACE_POSIX

#else

#endif /* APP_TRACE_POSIX */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
typedef TASK_HANDLE_TYPE	event_loop_handle_t;

/// Configuration for creating event loops
typedef struct {
    int32_t queue_size;
    const char* task_name;

    UBaseType_t task_priority;
    uint32_t task_stack_size;
    BaseType_t task_core_id;

} esp_event_loop_args_t;

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Application basic definitions (mostly task related) */
typedef enum event_loop_defs_t {
	/*! Default events loop task priority */
	EVENT_LOOP_PRIORITY = APP_MAX_PRIORITY - 5,
	/*****************************************************************/
	/*! Default events loop stack size */
	EVENT_LOOP_STACK_SIZE = 2 * APP_BASE_STACK_SIZE + APP_BASE_STACK_EXTRA,
	/*****************************************************************/
	/*! Defaults events loop queue size */
	EVENT_LOOP_QUEUE_SIZE = 32 * APPS_BASE_QUEUE_SIZE,
	MAX_EVENTS = MAX_WIFI_EVENTS + MAX_IP_EVENTS,
} event_loop_defs_t;

static app_event_handler_t event_handlers[MAX_EVENTS] = { [0 ... MAX_EVENTS - 1] = NULL};
static event_loop_handle_t s_default_loop = NULL;
static bool is_ready = false;
static app_req_res_queues_t event_loop_posix_queues = { NULL };

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static bool event_loop_posix_handler(event_loop_posix_req_t *p_from) {
	CONTINUE_CONDITION(p_from != NULL, false);
	CONTINUE_CONDITION(event_handlers[APP_EVENT_ANY_ID] != NULL, false);
	event_handlers[APP_EVENT_ANY_ID](NULL, p_from->event_base, p_from->event_id, NULL);

	return true;
}

static void event_loop_posix_task(void* args)
{
	APP_LOG(LOG_WARN, FMT_ARGS("running task for loop %p", s_default_loop));

	event_loop_posix_req_t req = {
		.event_base = NULL,
		.event_id = 0
	};

	is_ready = true;

	for (;;) {
		if (QUEUE_PULL(event_loop_posix_queues.p_req_handle, &req, MS(100)) == pdFAIL) {
			continue;
		}

		if (event_loop_posix_handler(&req) == false) {
			continue;
		}
	}
}

static app_err_t event_loop_posix_create(const esp_event_loop_args_t *event_loop_args,
                                         event_loop_handle_t *event_loop) {
	event_loop_posix_req_t event_loop_posix_req_buffer[event_loop_args->queue_size];
	static StaticQueue_t event_loop_posix_req_queue;

	event_loop_posix_queues.p_req_handle = QUEUE_STATIC(event_loop_posix_req_buffer,
	                                                    &event_loop_posix_req_queue);

	CONTINUE_CONDITION(event_loop_posix_queues.p_req_handle != NULL, APP_FAIL);

	if (TASK_DYNAMIC(event_loop_posix_task,
	                 event_loop_args->task_stack_size,
	                 NULL,
	                 event_loop_args->task_priority,
	                 event_loop) == pdFALSE) {
		return APP_FAIL;
	}

	return APP_OK;
}

app_err_t event_loop_port_create_default() {
	CONTINUE_CONDITION(s_default_loop == NULL, APP_FAIL);

  esp_event_loop_args_t loop_args = {
      .queue_size = EVENT_LOOP_QUEUE_SIZE,
      .task_name = NULL,
      .task_stack_size = EVENT_LOOP_STACK_SIZE,
      .task_priority = EVENT_LOOP_PRIORITY,
      .task_core_id = 0
  };

  return event_loop_posix_create(&loop_args, &s_default_loop);
}

app_err_t event_loop_port_handler_register(app_event_base_t event_base,
                                           int32_t event_id,
                                           void *event_handler,
                                           void *event_handler_arg) {
	event_handlers[event_id] = event_handler;

	return APP_OK;
}

app_err_t event_loop_port_handler_unregister(app_event_base_t event_base,
                                             int32_t event_id,
                                             void *event_handler) {
	event_handlers[event_id] = NULL;

	return APP_OK;
}

app_err_t event_loop_port_delete_default() {
	TASK_DELETE(s_default_loop);
	s_default_loop = NULL;

	vQueueDelete(event_loop_posix_queues.p_req_handle);
	event_loop_posix_queues.p_req_handle = NULL;

	APP_LOG(LOG_INFO, FMT_NONE("event loop deleted"));

	return APP_OK;
}

bool event_loop_posix_req(event_loop_posix_req_t *p_from, uint32_t timeout_ms) {
	if (is_ready == false) {
		return false;
	}

	if (QUEUE_PUSH(p_from, event_loop_posix_queues.p_req_handle, timeout_ms) == pdFAIL) {
	 return false;
	}

	return true;
}
