///@file fan_control_application.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

#define FAN_CONTROL_APP_GET_SPEED(SPEED, STATUS)	\
	do{	\
		for (uint32_t i = 0; i < 4; i++) {	\
			if (((((STATUS) & 0x0f) >> i) & 1) == 0) {	\
				continue;	\
			}	\
			(SPEED) = i;	\
			break;\
		}	\
	} while(0);

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "fan_control_application.h"
#include "soft_wdt_application.h"
#include "servo_phy_layer.h"
#include "proprietary_utils.h"
#include "app_nvs.h"


#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum fan_control_app_states_t {
	FAN_CONTROL_SPD_OFF_STATE = (1 << 0),
	FAN_CONTROL_SPD_MIN_STATE = (1 << 1),
	FAN_CONTROL_SPD_MED_STATE = (1 << 2),
	FAN_CONTROL_SPD_MAX_STATE = (1 << 3),
	FAN_CONTROL_OSC_OFF_STATE = (1 << 4),
	FAN_CONTROL_OSC_ON_STATE = (1 << 5),
} fan_control_app_states_t;

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Application main RTOS task
 * @param p_arg Address of arguments passed on task execution
 */
static void fan_control_app_task(void *p_arg);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static uint32_t wait_timeout = 0;
static TASK_HANDLE_TYPE task_handle = NULL;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#define FAN_CONTROL_APP_FAN_STS_DEFAULT				0x11 /* 0b00010001 (Fan oscillating on and speed off by default) */
#define FAN_CONTROL_APP_FAN_STS_FILE					".fan_sts"

static void fan_control_app_task(void *p_arg) {
	fan_control_app_req_t req = 0;
	servo_phy_continuous_params_t to;

	uint32_t fan_states = app_nvs_read_u32_with_backup(FAN_CONTROL_APP_FAN_STS_FILE);
	if (fan_states == (uint32_t )-1) {
		fan_states = FAN_CONTROL_APP_FAN_STS_DEFAULT;
		app_nvs_write_u32_with_backup(fan_states, FAN_CONTROL_APP_FAN_STS_FILE);
	}
	APP_LOG(LOG_WARN, FMT_ARGS("fan_states: 0x%02X", fan_states));

	uint32_t prev_states = fan_states;
	for (;;) {
		if (prev_states != fan_states) {
			prev_states = fan_states;
			app_nvs_write_u32_with_backup(fan_states, FAN_CONTROL_APP_FAN_STS_FILE);
		}

		if (TASK_WAIT_NOTIFY((uint32_t *)&req, wait_timeout) == false) {
			soft_wdt_app_kick(FAN_CONTROL_APP_IDX);
			continue;
		}

		APP_LOG(LOG_WARN, FMT_ARGS("Notification: %u", req));

		switch (req) {
			case FAN_CONTROL_SPD_OFF_REQ: {
				to.speed_pct = 100;
				to.direction = 1;

				if ((fan_states & FAN_CONTROL_SPD_OFF_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Already Off"));
					break;
				}

				uint32_t speed = 0;
				FAN_CONTROL_APP_GET_SPEED(speed, fan_states);
				APP_LOG(LOG_WARN, FMT_ARGS("Speed: %u", speed));
				to.time_ms = 350 * speed;

				if (servo_phy_set_rot(SERVO_0, &to) != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Off Fail"));
				}

				fan_states &= 0xf0;
				fan_states |= FAN_CONTROL_SPD_OFF_STATE;
				break;
			}
			case FAN_CONTROL_SPD_MIN_REQ: {
				to.speed_pct = 100;

				if ((fan_states & FAN_CONTROL_SPD_MIN_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Already At Minimun"));
					break;
				}

				uint32_t speed = 0;
				FAN_CONTROL_APP_GET_SPEED(speed, fan_states);
				APP_LOG(LOG_WARN, FMT_ARGS("Curr Speed: %u", speed));

				to.direction = 0;
				to.time_ms = 350;

				if (speed > 1) {
					to.direction = 1;
					to.time_ms = 350 * (speed - 1);
				}

				if (servo_phy_set_rot(SERVO_0, &to) != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Min Fail"));
				}

				fan_states &= 0xf0;
				fan_states |= FAN_CONTROL_SPD_MIN_STATE;
				break;
			}
			case FAN_CONTROL_SPD_MED_REQ: {
				to.speed_pct = 100;

				if ((fan_states & FAN_CONTROL_SPD_MED_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Already At Medium"));
					break;
				}

				uint32_t speed = 0;
				FAN_CONTROL_APP_GET_SPEED(speed, fan_states);
				APP_LOG(LOG_WARN, FMT_ARGS("Curr Speed: %u", speed));

				to.direction = 0;
				to.time_ms = 350 * (2 - speed);

				if (speed > 2) {
					to.direction = 1;
					to.time_ms = 350 * (speed - 2);
				}

				if (servo_phy_set_rot(SERVO_0, &to) != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Med Fail"));
				}

				fan_states &= 0xf0;
				fan_states |= FAN_CONTROL_SPD_MED_STATE;
				break;
			}
			case FAN_CONTROL_SPD_MAX_REQ: {
				to.speed_pct = 100;

				if ((fan_states & FAN_CONTROL_SPD_MAX_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Already At Maximun"));
					break;
				}

				uint32_t speed = 0;
				FAN_CONTROL_APP_GET_SPEED(speed, fan_states);
				APP_LOG(LOG_WARN, FMT_ARGS("Curr Speed: %u", speed));

				to.direction = 0;
				to.time_ms = 350 * (3 - speed);

				if (servo_phy_set_rot(SERVO_0, &to) != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_NONE("Speed Max Fail"));
				}

				fan_states &= 0xf0;
				fan_states |= FAN_CONTROL_SPD_MAX_STATE;
				break;
			}
			case FAN_CONTROL_OSC_OFF_REQ: {
				to.speed_pct = 50;
				to.direction = 1;
				to.time_ms = 1000;

				if ((fan_states & FAN_CONTROL_OSC_OFF_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Oscillate Already Off"));
					break;
				}

				uint32_t ret = servo_phy_set_rot(SERVO_1, &to);
				if (ret != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_ARGS("Oscillate Off Fail, reason: %u", ret));
				}

				fan_states &= 0x0f;
				fan_states |= FAN_CONTROL_OSC_OFF_STATE;
				break;
			}
			case FAN_CONTROL_OSC_ON_REQ: {
				to.speed_pct = 50;
				to.direction = 0;
				to.time_ms = 1000;

				if ((fan_states & FAN_CONTROL_OSC_ON_STATE) != 0) {
					APP_LOG(LOG_ERROR, FMT_NONE("Oscillate Already On"));
					break;
				}

				uint32_t ret = servo_phy_set_rot(SERVO_1, &to);
				if (ret != SERVO_PHY_OK) {
					APP_LOG(LOG_ERROR, FMT_ARGS("Oscillate On Fail, reason: %u", ret));
				}

				fan_states &= 0x0f;
				fan_states |= FAN_CONTROL_OSC_ON_STATE;
				break;
			}
			case FAN_CONTROL_OSC_TOGGLE_REQ: {
				if ((fan_states & FAN_CONTROL_OSC_OFF_STATE) != 0) {
					(void) fan_control_app_req(FAN_CONTROL_OSC_ON_REQ);
					break;
				}

				if ((fan_states & FAN_CONTROL_OSC_ON_STATE) != 0) {
					(void) fan_control_app_req(FAN_CONTROL_OSC_OFF_REQ);
					break;
				}
				break;
			}
			default:
				break;
		}

		soft_wdt_app_kick(FAN_CONTROL_APP_IDX);
	}

	return;
}

void fan_control_app_cfg(uint32_t timeout_ms) {
	wait_timeout = timeout_ms;
	(void) servo_phy_setup();

  return;
}

void fan_control_app_run(void) {
	CONTINUE_CONDITION(task_handle == NULL, );

	APP_LOG(LOG_WARN, FMT_NONE("Starting fan_control_app"));

	(void) TASK_DYNAMIC(fan_control_app_task,
	                    FAN_CONTROL_APP_STACK_SIZE,
	                    NULL,
	                    FAN_CONTROL_APP_PRIORITY,
	                    &task_handle);
  return;
}

void fan_control_app_kill(void) {
	CONTINUE_CONDITION(task_handle != NULL, );

	APP_LOG(LOG_WARN, FMT_NONE("Killing fan_control_app"));

	TASK_DELETE(task_handle);
	task_handle = NULL;

	return;
}

void fan_control_app_req_from_isr(fan_control_app_req_t type) {
  TASK_NOTIFY_ISR(task_handle, type);

  return;
}

bool fan_control_app_req(fan_control_app_req_t type) {
  if (TASK_NOTIFY(task_handle, type) != pdPASS) {
    return false;
  }

  return true;
}
