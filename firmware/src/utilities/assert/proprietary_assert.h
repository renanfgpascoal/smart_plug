/// @file

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************
 * INCLUDES
 *******************************************************************/

#include <stdbool.h>
#include <stddef.h>

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

/*!
 * @brief Function continue contidion
 * @param TRUE Expected true statement
 * @param ELSE Calling function return in case TRUE is a false assumption
 */
#define CONTINUE_CONDITION(TRUE, ELSE)       if (!(TRUE)) { return ELSE; }

/*!
 * @brief Executes a cleanup routine if a given condition is false
 * @param CONDITION Expected true condition
 * @param CLEAN_UP Clean up routine to execute
 * example: CLEAN_UP_IF_FALSE(1 == 0, clean_up(); return false;)
 */
#define CLEAN_UP_IF_FALSE(CONDITION, CLEAN_UP)		if (!(CONDITION)) { CLEAN_UP }

/*!
 * @brief Macro to be called in assertion in case of void returning functions
 */
#define VOID_RETURN

#if defined (UNIT_TESTS)

/*!
 * @brief Function parameters assertion macro, enabled at Unit Tests environment
 * @param TRUE Expected true statement
 * @param ELSE Calling function return in case TRUE is a false assumption
 */
#define DEVELOPMENT_ASSERT(TRUE, ELSE)  	 CONTINUE_CONDITION(TRUE, ELSE)

#else /* UNIT_TESTS */

/*!
 * @brief Function parameters assertion macro, disabled outside of Unit Tests environment
 * @param TRUE Expected true statement
 * @param ELSE Calling function return in case TRUE is a false assumption
 */
#define DEVELOPMENT_ASSERT(TRUE, ELSE)     CONTINUE_CONDITION(TRUE, ELSE)

#endif /* UNIT_TESTS */

/*
 * Using preprocessor to simulate static_assert() on compilers without C11 support */
/* does not support 2 static_assert() on same line
 * https://github.com/Cyan4973/Writing_Safer_C_code/blob/master/5_compile_time_tests.md
 */

/*! Token pasting macro */
#define TOKENPASTE(a, b) a ## b /* "##" is the "Token Pasting Operator" */

/*! Expand and paste tokens */
#define EXPAND_THEN_PASTE(a,b) TOKENPASTE(a, b) /* expand then paste */

/*! Performs static assertions */
#define static_assert(x, msg) \
		enum { EXPAND_THEN_PASTE(ASSERT_line_,__LINE__) = 1 / ((msg) && (x)) }

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
