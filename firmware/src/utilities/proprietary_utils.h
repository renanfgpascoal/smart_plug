/// @file proprietary_utils.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Checks if a number is a power of two (not considering zero)
 * @param NUMBER Number to check
 */
#define IS_POWER_OF_TWO(NUMBER)						(((NUMBER) & ((NUMBER) - 1)) == 0)

/*!
 * @brief Generates kilobytes from a number
 * @param NUMBER Number to convert to kilobytes
 */
#define KB(NUMBER) 												((NUMBER) << 10)

/*!
 * @brief Get the number of elements in an array of items
 * @param ARRAY Name of the array to count elements
 * @return Number of elements
 */
#define ARRAY_SIZE(ARRAY) 								((sizeof(ARRAY)) / (sizeof((ARRAY)[0])))

/*!
 * @brief Get the number of elements in an list of items
 * @param LIST Name of the list to count elements
 * @return Number of elements
 */
#define LIST_SIZE(LIST)   								ARRAY_SIZE(LIST)

/*!
 * @brief Handles unused data
 * @param PARAM Parameter to be handled
 */
#define UNUSED(PARAM)     								(void)PARAM

/*!
 * @brief Wrapped to compute Kilo bytes
 * @param NUMBER Number of Kilo bytes
 */
#define KBYTES(NUMBER)  									((NUMBER) * (1024))

/*!
 * @brief Wrapper to 'quote' some text
 * @param X Text to quote
 */
#define QUOTE_ME(X) 											#X

/*!
 * @brief Quotes some text
 * @param X Text to quote
 */
#define STR(X) 														QUOTE_ME(X)

/*!
 * @brief Converts amount of counts in milliseconds
 * @param FROM Amount of counts to convert
 */
#define MS(FROM)													(FROM)

/*!
 * @brief Converts from Seconds to Milliseconds
 * @param FROM Amount of seconds convert
 */
#define MS_FROM_SECONDS(FROM)							MS((FROM) * 1000)

/*!
 * @brief Converts from Minutes to Milliseconds
 * @param FROM Amount of minutes convert
 */
#define MS_FROM_MINUTES(FROM)							MS_FROM_SECONDS((FROM) * 60)

/*!
 * @brief Converts from Seconds to Milliseconds
 * @param FROM Amount of hours convert
 */
#define MS_FROM_HOURS(FROM)								MS_FROM_MINUTES((FROM) * 60)

/*!
 * @brief Converts from minutes to Seconds
 * @param FROM Amount of minutes to convert
 */
#define SECONDS_FROM_MS(FROM)						((FROM) / 1000)

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <string.h>
#include <stdint.h>

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

/*!
 * @brief Determines the size of a member of a structure
 * @param TYPE Type of the structure to get member
 * @param MEMBER Name o the member of the structure
 */
#define STRUCTURE_MEMBER_SIZE(TYPE, MEMBER)	sizeof(((TYPE *)0)->MEMBER)

/*!
 * @brief Builds a String Descriptor from a given string
 * @param FROM Name of the string buffer to descript
 */
#define STRING_DESCRIPT(FROM)		{ .p_addr = &(FROM)[0], .size = sizeof(FROM) }

/*!
 * @brief Initializes a string
 * @param Size Size of buffer containing a string
 */
#define INIT_STRING(SIZE) {  [0 ... SIZE - 2] = 0xFF, [SIZE - 1] = '\0' }

/*!
 * @brief Sets NULL terminator of a string buffer
 * @param P_FROM Address of string buffer
 * @param OFFSET Offset NULL terminator position
 */
#define SET_NULL_TERM_STRING(P_FROM, OFFSET) \
		(void) memset((void *)((P_FROM) + (OFFSET) - 1), '\0', 1)

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Describes a string variable, to be used with strnXXX functions */
typedef struct string_descriptor_t {
	/*! Address of the string */
 const char *p_addr;
 /*! Size of the character buffer holding the string */
 const uint32_t size;
} string_descriptor_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
