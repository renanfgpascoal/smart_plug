/// @file posix_support.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <stdint.h>
#include <signal.h>

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef __unix__

#define POSIX_ALLOW_SIGINT()	\
	(void) signal( SIGINT, ext_command_exit )

#else

#define POSIX_ALLOW_SIGINT()

#endif /* __unix__ */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

/*
 * @brief Signal handler for Ctrl_C to cause the program to exit, and generate the
 * profiling info.
 * @param signal command signal
 */
void ext_command_exit( int signal );

/*
 * @brief Gets external time in seconds
 * @return time in seconds if success, UINT32_MAX if fail
 */
uint32_t ext_time_get();

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
