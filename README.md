# Simple Smart Plug Project
**Smart plug based on ESP8266**
## Features:
1. Remotely turn on/off loads till 10A;
2. Automatically turn on/off notebook charger when battery is full.
## Build project steps:
1. git submodule update --init --recursive
2. Go to smart_plug/firmware/third_party/esp/esp8266/
3. ./install.sh
4. export IDF_PATH="/home/renanfgpascoal/Documentos/PERSONAL_REPO/smart_plug/firmware/third_party/esp/esp8266"
5. . ./export.sh
6. sudo apt-get install libncurses5-dev libncursesw5-dev flex bison gperf
7. Go to smart_plug/firmware/portable/esp8266/idf/
8. idf.py build
